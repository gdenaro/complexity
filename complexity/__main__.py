from complexity.executors import bruteforce_executor
from complexity.executors import wise_executor
from complexity.executors import genetic_executor
from complexity.utils import model_dict_to_string

import click
import arrow

import functools
import csv


EXPERIMENT_OUTPUT_CSV_FIELDS = [
    'method',
    'algorithm',
    'size',
    'run',
    'worst_case_model',
    'worst_case_input',
    'worst_case_cost',
    'method_execution_time',
]


def write_experiment_output(file, method, algorithm, run, size, worst_case_model, worst_case_input, worst_case_cost,
                            method_execution_time):
    csv_writer = csv.DictWriter(file, EXPERIMENT_OUTPUT_CSV_FIELDS, quoting=csv.QUOTE_NONNUMERIC, lineterminator='\n')
    if file.tell() == 0:
        csv_writer.writeheader()
    csv_writer.writerow({
        'method': method,
        'algorithm': algorithm,
        'run': run,
        'size': size,
        'worst_case_model': worst_case_model,
        'worst_case_input': worst_case_input,
        'worst_case_cost': worst_case_cost,
        'method_execution_time': method_execution_time,
    })


def compute_execution_time(start_time):
    return int((arrow.utcnow() - start_time).total_seconds() * 1000)


def common_parameters(func):
    @click.argument('module', type=str, required=True)
    @click.argument('function', type=str, required=True)
    @click.argument('size', type=int, default=10)
    @click.option('--args-gen', '-A', 'args_gen', type=str, default='args_gen',
                  help='The name of the function to generate positional arguments.')
    @click.option('--kwargs-gen', '-K', 'kwargs_gen', type=str, default='kwargs_gen',
                  help='The name of the function to generate keyword arguments.')
    @click.option('--preconditions-gen', '-P', 'preconditions_gen', type=str, default='preconditions_gen',
                  help='The name of the function to generate preconditions.')
    @click.option('--model-to-input', '-I', 'model_to_input', type=str, default='model_to_input',
                  help='The name of the function to generate preconditions.')
    @click.option('--timeout', '-T', 'timeout', type=float, default=1.0,
                  help='The timeout in seconds for the symbolic execution.')
    @click.option('--experiments-outputs-csv', '-o', 'experiments_outputs_csv', type=click.File('a'), required=False,
                  help='The CSV file where appending the experiments reports.')
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        return func(*args, **kwargs)
    return wrapper


@click.group()
def cli():
    pass


@cli.command()
@common_parameters
def bruteforce(module, function, size, args_gen, kwargs_gen, preconditions_gen, model_to_input, timeout, experiments_outputs_csv):
    configuration = bruteforce_executor.Configuration(
        module=module,
        function=function,
        size=size,
        args_gen=args_gen,
        kwargs_gen=kwargs_gen,
        preconditions_gen=preconditions_gen,
        model_to_input=model_to_input,
        timeout=timeout,
    )

    method_execution_start_time = arrow.utcnow()
    worst_case_model, worst_case_input, worst_case_cost = bruteforce_executor.run(configuration)
    method_execution_time = compute_execution_time(method_execution_start_time)

    if experiments_outputs_csv:
        write_experiment_output(
            file=experiments_outputs_csv,
            method='brute_force',
            algorithm=function,
            run=arrow.utcnow().timestamp,
            size=size,
            worst_case_model=worst_case_model,
            worst_case_input=worst_case_input,
            worst_case_cost=worst_case_cost,
            method_execution_time=method_execution_time,
        )

    print(f'worst case model: {worst_case_model}')
    print(f'worst case input: {worst_case_input}')
    print(f'worst_case_cost: {worst_case_cost}')


@cli.command()
@common_parameters
@click.option('--min-training-size', '-m', 'min_training_size', type=int, default=0,
              help='The minimum size for the training phase.')
@click.option('--max-training-size', '-M', 'max_training_size', type=int, default=9,
              help='The maximum size for the training phase.')
def wise(module, function, size, args_gen, kwargs_gen, preconditions_gen, model_to_input, timeout, experiments_outputs_csv,
         min_training_size, max_training_size):
    configuration = wise_executor.Configuration(
        module=module,
        function=function,
        size=size,
        args_gen=args_gen,
        kwargs_gen=kwargs_gen,
        preconditions_gen=preconditions_gen,
        model_to_input=model_to_input,
        timeout=timeout,
        min_training_size=min_training_size,
        max_training_size=max_training_size,
        history_size=0,
    )

    method_execution_start_time = arrow.utcnow()
    worst_case_model, worst_case_input, worst_case_cost = wise_executor.run(configuration)
    method_execution_time = compute_execution_time(method_execution_start_time)

    if experiments_outputs_csv:
        write_experiment_output(
            file=experiments_outputs_csv,
            method='wise',
            algorithm=function,
            run=arrow.utcnow().timestamp,
            size=size,
            worst_case_model=worst_case_model,
            worst_case_input=worst_case_input,
            worst_case_cost=worst_case_cost,
            method_execution_time=method_execution_time,
        )

    print(f'worst case model: {worst_case_model}')
    print(f'worst case input: {worst_case_input}')
    print(f'worst_case_cost: {worst_case_cost}')


@cli.command()
@common_parameters
@click.option('--min-training-size', '-m', 'min_training_size', type=int, default=0,
              help='The minimum size for the training phase.')
@click.option('--max-training-size', '-M', 'max_training_size', type=int, default=9,
              help='The maximum size for the training phase.')
def corina(module, function, size, args_gen, kwargs_gen, preconditions_gen, model_to_input, timeout, experiments_outputs_csv,
           min_training_size, max_training_size):
    configuration = wise_executor.Configuration(
        module=module,
        function=function,
        size=size,
        args_gen=args_gen,
        kwargs_gen=kwargs_gen,
        preconditions_gen=preconditions_gen,
        model_to_input=model_to_input,
        timeout=timeout,
        min_training_size=min_training_size,
        max_training_size=max_training_size,
        history_size=1,
    )

    method_execution_start_time = arrow.utcnow()
    worst_case_model, worst_case_input, worst_case_cost = wise_executor.run(configuration)
    method_execution_time = compute_execution_time(method_execution_start_time)

    if experiments_outputs_csv:
        write_experiment_output(
            file=experiments_outputs_csv,
            method='corina',
            algorithm=function,
            run=arrow.utcnow().timestamp,
            size=size,
            worst_case_model=worst_case_model,
            worst_case_input=worst_case_input,
            worst_case_cost=worst_case_cost,
            method_execution_time=method_execution_time,
        )

    print(f'worst case model: {worst_case_model}')
    print(f'worst case input: {worst_case_input}')
    print(f'worst_case_cost: {worst_case_cost}')


@cli.command()
@common_parameters
@click.option('--seed', '-s', 'seed', type=int, default=0, help='The seed for the random generator.')
@click.option('--population-size', '-p', 'population_size', type=int, default=10, help='The population size.')
@click.option('--generations', '-g', 'generations', type=int, default=2, help='The number of generations.')
@click.option('--crossover', '-c', 'crossover', type=click.Choice(['singlepoint', 'prefix', 'exclude', 'union']),
              default='singlepoint', help='The type of crossover.')
@click.option('--mutation-probability', '-m', 'mutation_probability', type=float, default=0.2,
              help='The mutation probability.')
@click.option('--elitism-rate', '-e', 'elitism_rate', type=float, default=0.1, help='The elitism rate.')
@click.option('--processors', '-P', 'processors', type=int, default=16,
              help='The number of multiple processors to compute in parallel.')
@click.option('--random-search', '-R', 'random_search', is_flag=True,
              help='Runs the algorithms using the random search method.')
@click.option('--evolution-csv', '-O', 'evolution_csv', type=click.File('w'), required=False,
              help='The CSV file where writing the evolution reports.')
def genetic(module, function, size, args_gen, kwargs_gen, preconditions_gen, model_to_input, timeout,
            experiments_outputs_csv, seed, population_size, generations, crossover, mutation_probability, elitism_rate,
            processors, random_search, evolution_csv):
    configuration = genetic_executor.Configuration(
        module=module,
        function=function,
        size=size,
        args_gen=args_gen,
        kwargs_gen=kwargs_gen,
        preconditions_gen=preconditions_gen,
        model_to_input=model_to_input,
        timeout=timeout,
        seed=seed,
        population_size=population_size,
        generations=generations,
        crossover=crossover,
        mutation_probability=mutation_probability,
        elitism_rate=elitism_rate,
        processors=processors,
        random_search=random_search,
        evolution_csv=evolution_csv,
    )

    method_execution_start_time = arrow.utcnow()
    worst_case_model, worst_case_input, worst_case_cost = genetic_executor.run(configuration)
    method_execution_time = compute_execution_time(method_execution_start_time)

    if random_search:
        method = 'random_search'
    else:
        method = f'genetic_algorithm_{crossover}'

    if experiments_outputs_csv:
        write_experiment_output(
            file=experiments_outputs_csv,
            method=method,
            algorithm=function,
            run=seed,
            size=size,
            worst_case_model=worst_case_model,
            worst_case_input=worst_case_input,
            worst_case_cost=worst_case_cost,
            method_execution_time=method_execution_time,
        )

    print(f'worst case model: {worst_case_model}')
    print(f'worst case input: {worst_case_input}')
    print(f'worst_case_cost: {worst_case_cost}')


if __name__ == '__main__':
    cli()
