from complexity.structs import slist
from complexity.symex import SymbolicExecutor
from complexity.utils import pp_wcet_profiles
from complexity.utils import stopwatch
from complexity.executors.bruteforce_executor import analyze_wcet
from complexity.executors.bruteforce_executor import wcetpp


def args_gen(size):
    return slist('l', size),


def kwargs_gen(size):
    return {}


def preconditions_gen(size, args, kwargs):
    return []


def algox(l):
    """Returns `True` if any triple of numbers in A sums up to zero.

    Note:
        Expected worst case: the input list contains no triple of numbers that sums up to zero.
    """
    return algoxr(l, 0, 0, 2)


def algoxr(l, t, i, r):
    while i < len(l):
        if r == 0:
            if l[i] == t:
                return True
        elif algoxr(l, t - l[i], i + 1, r - 1):
            return True
        i += 1
    return False


if __name__ == '__main__':
    with stopwatch('wcetpp'):
        size = 5
        args = args_gen(size)
        kwargs = kwargs_gen(size)

        se = SymbolicExecutor()
        se.preconditions = preconditions_gen(size, args, kwargs)
        se.timeout = 10

        profiles = wcetpp(
            se,
            algox,
            args,
            kwargs,
        )
    pp_wcet_profiles(profiles)

    with stopwatch('analyze_wcet'):
        profiles = analyze_wcet(
            algox,
            args_gen,
            kwargs_gen,
            preconditions_gen,
            min_training_size=0,
            max_training_size=10,
            timeout=5,
            target_size=15,
            history_size=1,
        )
    pp_wcet_profiles(profiles)
