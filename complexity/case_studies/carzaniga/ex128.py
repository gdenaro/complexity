from complexity.structs import slist
from complexity.symex import SymbolicExecutor
from complexity.utils import assert_all_positive
from complexity.utils import pp_wcet_profiles
from complexity.utils import stopwatch
from complexity.executors.bruteforce_executor import analyze_wcet
from complexity.executors.bruteforce_executor import wcetpp


def args_gen(size):
    return slist('l', size),


def kwargs_gen(size):
    return {}


def preconditions_gen(size, args, kwargs):
    return [assert_all_positive(args[0])]


def algox(l):
    """Return `True` iff there are a,b,c in l such that b - a == c - b.

    Expected worst case: the input list contain no a,b,c in l such that b - a == c - b.
    """
    for i in range(2, len(l)):
        for j in range(1, i):
            for k in range(0, j):
                x = l[i]
                y = l[j]
                z = l[k]
                if x > y:
                    x, y = y, x
                if y > z:
                    y, z = z, y
                    if x > y:
                        x, y = y, x
                if y - x == z - y:
                    return True
    return False


if __name__ == '__main__':
    with stopwatch('wcetpp'):
        size = 4
        args = args_gen(size)
        kwargs = kwargs_gen(size)

        se = SymbolicExecutor()
        se.preconditions = preconditions_gen(size, args, kwargs)
        se.timeout = 10

        profiles = wcetpp(
            se,
            algox,
            args,
            kwargs,
        )
    pp_wcet_profiles(profiles)

    with stopwatch('analyze_wcet'):
        profiles = analyze_wcet(
            algox,
            args_gen,
            kwargs_gen,
            preconditions_gen,
            min_training_size=0,
            max_training_size=5,
            timeout=5,
            target_size=15,
            history_size=1,
        )
    pp_wcet_profiles(profiles)
