import z3

from complexity.structs import slist
from complexity.symex import SymbolicExecutor
from complexity.utils import pp_wcet_profiles
from complexity.utils import stopwatch
from complexity.executors.bruteforce_executor import analyze_wcet
from complexity.executors.bruteforce_executor import wcetpp


def args_gen(size):
    return slist('l', size),


def kwargs_gen(size):
    return {}


def preconditions_gen(size, args, kwargs):
    return [z3.Or(e == 0, e == 1) for e in args[0]]


def algoy(l):
    """Return the length of the longest subsequence of A that contains as many even numbers as odd numbers.

    Note:
        Expected worst case: a list containing as many even as odd numbers.
    """
    a = 0
    for i in range(len(l) - 1):
        for j in range(i+1, len(l)):
            x = 0
            for k in range(i, j+1):
                if l[k] % 2 == 0:
                    x += 1
                else:
                    x -= 1
            if x == 0 and j-i > a:
                a = j-i
    return a


if __name__ == '__main__':
    with stopwatch('wcetpp'):
        size = 4
        args = args_gen(size)
        kwargs = kwargs_gen(size)

        se = SymbolicExecutor()
        se.preconditions = preconditions_gen(size, args, kwargs)
        se.timeout = 10

        profiles = wcetpp(
            se,
            algoy,
            args,
            kwargs,
        )
    pp_wcet_profiles(profiles)

    # Non-deterministic policy.
    with stopwatch('analyze_wcet'):
        profiles = analyze_wcet(
            algoy,
            args_gen,
            kwargs_gen,
            preconditions_gen,
            min_training_size=0,
            max_training_size=4,
            timeout=10,
            target_size=5,
            history_size=1
        )
    pp_wcet_profiles(profiles)
