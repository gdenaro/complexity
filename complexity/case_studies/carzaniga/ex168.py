import z3

from complexity.symex import SymbolicExecutor
from complexity.utils import assert_in_range
from complexity.utils import pp_wcet_profiles
from complexity.utils import stopwatch
from complexity.executors.bruteforce_executor import wcetpp


def args_gen(size):
    return z3.Int('n'),


def kwargs_gen(size):
    return {}


def preconditions_gen(size, args, kwargs):
    return [assert_in_range(args[0], 0, (1 << size))]


def algox(n):
    """Ask Antonio Carzaniga about this.

    Note:
        Expected worst case: unknown.
    """
    c = 0
    a = n
    while a > 1:
        b = 1
        while b <= a*a:
            c += 1
            b *= 2
        a /= 2
    return c


if __name__ == '__main__':
    with stopwatch('wcetpp'):
        size = 4
        args = args_gen(size)
        kwargs = kwargs_gen(size)

        se = SymbolicExecutor()
        se.preconditions = preconditions_gen(size, args, kwargs)
        se.timeout = 10

        profiles = wcetpp(
            se,
            algox,
            args,
            kwargs,
        )
    pp_wcet_profiles(profiles)
