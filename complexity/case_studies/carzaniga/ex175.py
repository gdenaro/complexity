from complexity.structs import slist
from complexity.symex import SymbolicExecutor
from complexity.utils import assert_in_range
from complexity.utils import pp_wcet_profiles
from complexity.utils import stopwatch
from complexity.executors.bruteforce_executor import analyze_wcet
from complexity.executors.bruteforce_executor import wcetpp


def args_gen(size):
    return slist('l', size),


def kwargs_gen(size):
    return {}


def preconditions_gen(size, args, kwargs):
    # Enforce all list items in the range [1, 2] to bound the symbolic execution of `algox`.
    return [assert_in_range(e, 1, 2) for e in args[0]]


def algox(l):
    """Calculate the product of the numbers in A.

    Note:
        Expected worst case: a list containing n occurrences of the maximum number that can be represented.
    """
    B = list(l)
    i = 0
    x = 1
    while i < len(l):
        B[i] -= 1
        if B[i] == 0:
            B[i] = l[i]
            i += 1
        else:
            x += 1
            i = 0
    return x


if __name__ == '__main__':
    with stopwatch('wcetpp'):
        size = 5
        args = args_gen(size)
        kwargs = kwargs_gen(size)

        se = SymbolicExecutor()
        se.preconditions = preconditions_gen(size, args, kwargs)
        se.timeout = 10

        profiles = wcetpp(
            se,
            algox,
            args,
            kwargs,
        )
    pp_wcet_profiles(profiles)

    # Non-deterministic policy.
    with stopwatch('analyze_wcet'):
        profiles = analyze_wcet(
            algox,
            args_gen,
            kwargs_gen,
            preconditions_gen,
            min_training_size=0,
            max_training_size=4,
            timeout=5,
            target_size=5,
            history_size=1
        )
    pp_wcet_profiles(profiles)
