from complexity.structs import slist
from complexity.symex import SymbolicExecutor
from complexity.utils import pp_wcet_profiles
from complexity.utils import stopwatch
from complexity.executors.bruteforce_executor import analyze_wcet
from complexity.executors.bruteforce_executor import wcetpp


def args_gen(size):
    return slist('l', size),


def kwargs_gen(size):
    return {}


def preconditions_gen(size, args, kwargs):
    return []


def algoy(l):
    """Return the maximum distance between any two numbers in the list A.

    Note:
        Expected worst case: unknown.
    """
    i = 1
    j = 0
    x = -(1 << 32)
    while i < len(l):
        a = abs(l[i] - l[j])
        if a > x:
            x = a
        j += 1
        if j == i:
            i += 1
            j = 0
    return x


if __name__ == '__main__':
    with stopwatch('wcetpp'):
        size = 4
        args = args_gen(size)
        kwargs = kwargs_gen(size)

        se = SymbolicExecutor()
        se.preconditions = preconditions_gen(size, args, kwargs)
        se.timeout = 10

        profiles = wcetpp(
            se,
            algoy,
            args,
            kwargs,
        )
    pp_wcet_profiles(profiles)

    # Non-deterministic policy.
    with stopwatch('analyze_wcet'):
        profiles = analyze_wcet(
            algoy,
            args_gen,
            kwargs_gen,
            preconditions_gen,
            min_training_size=0,
            max_training_size=4,
            timeout=10,
            target_size=5,
            history_size=2
        )
    pp_wcet_profiles(profiles)
