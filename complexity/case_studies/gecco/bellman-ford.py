from complexity.structs import swgraph, WGraph
from complexity.symex import SymbolicExecutor
from complexity.utils import pp_wcet_profiles
from complexity.utils import stopwatch
from complexity.executors.bruteforce_executor import analyze_wcet
from complexity.executors.bruteforce_executor import wcetpp


import re


INF = 1 << 32


def args_gen(size):
    graph = swgraph('g', size)

    # Remove self-loops.
    for n in graph.nodes():
        graph.remove_edge(n, n)

    return graph, 0


def kwargs_gen(size):
    return {}


def preconditions_gen(size, args, kwargs):
    return []


def model_to_input(model, size):
    matrix = []
    for i in range(size):
        row = []
        for j in range(size):
            row.append(0)
        matrix.append(row)
    g = WGraph(matrix)

    for var, value in model.items():
        i, j = re.match(r'g\[(\d+)\]\[(\d+)\]', var).groups()
        g.add_edge(int(i), int(j), int(value))

    for n in g.nodes():
        g.remove_edge(n, n)

    return g, 0


def bellman_ford(g, source):
    distance = {}
    predecessor = {}

    vertices = g.nodes()
    for v in vertices:
        distance[v] = INF
        predecessor[v] = None

    distance[source] = 0

    edges = g.edges()
    for _ in range(1, len(vertices) - 1):
        for u, v, w in edges:
            if distance[u] + w < distance[v]:
                distance[v] = distance[u] + w
                predecessor[v] = u

    for u, v, w in edges:
        if distance[u] + w < distance[v]:
            return None

    return distance, predecessor


if __name__ == '__main__':
    with stopwatch('wcetpp'):
        size = 3
        args = args_gen(size)
        kwargs = kwargs_gen(size)

        se = SymbolicExecutor()
        se.preconditions = preconditions_gen(size, args, kwargs)
        se.timeout = 10

        profiles = wcetpp(
            se,
            bellman_ford,
            args,
            kwargs,
        )
    pp_wcet_profiles(profiles)

    # Non-deterministic policy.
    with stopwatch('analyze_wcet'):
        profiles = analyze_wcet(
            bellman_ford,
            args_gen,
            kwargs_gen,
            preconditions_gen,
            min_training_size=1,
            max_training_size=3,
            timeout=10,
            target_size=4,
            history_size=2
        )
    pp_wcet_profiles(profiles)
