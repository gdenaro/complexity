import z3

from complexity.structs import slist
from complexity.symex import SymbolicExecutor
from complexity.utils import assert_all_different
from complexity.utils import assert_all_positive
from complexity.utils import assert_sorted
from complexity.utils import pp_wcet_profiles
from complexity.utils import stopwatch
from complexity.executors.bruteforce_executor import analyze_wcet
from complexity.executors.bruteforce_executor import wcetpp


def args_gen(size):
    return slist('l', size), z3.Int('e')


def kwargs_gen(size):
    return {}


def preconditions_gen(size, args, kwargs):
    return [
        assert_all_positive(args[0]),
        assert_all_different(args[0]),
        assert_sorted(args[0])
    ]


def model_to_input(model, size):
    l = [0] * size
    for var, value in model.items():
        if var == 'e':
            e = int(value)
        else:
            index = int(var.strip('l[]'))
            l[index] = int(value)
    return l, e


def binary_search(l, e):
    first = 0
    last = len(l) - 1
    found = False
    while first <= last and not found:
        m = (first + last) // 2
        if l[m] == e:
            found = True
        else:
            if e < l[m]:
                last = m - 1
            else:
                first = m + 1
    return found


if __name__ == '__main__':
    with stopwatch('wcetpp'):
        size = 15
        args = args_gen(size)
        kwargs = kwargs_gen(size)

        se = SymbolicExecutor()
        se.preconditions = preconditions_gen(size, args, kwargs)
        se.timeout = 10

        profiles = wcetpp(
            se,
            binary_search,
            args,
            kwargs,
        )
    pp_wcet_profiles(profiles)

    with stopwatch('analyze_wcet'):
        profiles = analyze_wcet(
            binary_search,
            args_gen,
            kwargs_gen,
            preconditions_gen,
            min_training_size=0,
            max_training_size=15,
            timeout=5,
            target_size=20,
            history_size=1,
        )
    pp_wcet_profiles(profiles)
