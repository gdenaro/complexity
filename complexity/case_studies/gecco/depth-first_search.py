from complexity.structs import sgraph, Graph
from complexity.symex import SymbolicExecutor
from complexity.utils import pp_wcet_profiles
from complexity.utils import stopwatch
from complexity.executors.bruteforce_executor import analyze_wcet
from complexity.executors.bruteforce_executor import wcetpp


import re


def args_gen(size):
    graph = sgraph('g', size)

    # Remove self-loops.
    for n in graph.nodes():
        graph.remove_edge(n, n)

    return graph, 0, size-1


def kwargs_gen(size):
    return {}


def preconditions_gen(size, args, kwargs):
    return []


def model_to_input(model, size):
    matrix = []
    for i in range(size):
        row = []
        for j in range(size):
            row.append(False)
        matrix.append(row)
    g = Graph(matrix)

    for var, value in model.items():
        i, j = re.match(r'g\[(\d+)\]\[(\d+)\]', var).groups()
        if value:
            g.add_edge(int(i), int(j))

    return g, 0, size-1


def depth_first_search(g, a, b):
    """Determine if `b` is reachable from `a` in `g` using DFS.

    Note:
        Expected worst case: a graph in which all nodes must be explored before finding a path from a to b.
    """
    return _dfs(g, a, b, set())


def _dfs(g, a, b, visited):
    if a == b:
        return True

    visited.add(a)
    for n in g.neighbours(a):
        if n not in visited:
            if _dfs(g, n, b, visited):
                return True
    return False


if __name__ == '__main__':
    with stopwatch('wcetpp'):
        size = 4
        args = args_gen(size)
        kwargs = kwargs_gen(size)

        se = SymbolicExecutor()
        se.preconditions = preconditions_gen(size, args, kwargs)
        se.timeout = 10

        profiles = wcetpp(
            se,
            depth_first_search,
            args,
            kwargs,
        )
    pp_wcet_profiles(profiles)

    # Non-deterministic policy.
    with stopwatch('analyze_wcet'):
        profiles = analyze_wcet(
            depth_first_search,
            args_gen,
            kwargs_gen,
            preconditions_gen,
            min_training_size=1,
            max_training_size=4,
            timeout=30,
            target_size=5,
            history_size=2
        )
    pp_wcet_profiles(profiles)
