from complexity.structs import swgraph, WGraph
from complexity.symex import SymbolicExecutor
from complexity.utils import assert_in_range
from complexity.utils import pp_wcet_profiles
from complexity.utils import stopwatch
from complexity.executors.bruteforce_executor import analyze_wcet
from complexity.executors.bruteforce_executor import wcetpp


import re


INF = 1 << 32


def args_gen(size):
    graph = swgraph('g', size)

    # Remove self-loops.
    for n in graph.nodes():
        graph.remove_edge(n, n)

    return graph, 0


def kwargs_gen(size):
    return {}


def preconditions_gen(size, args, kwargs):
    preconditions = []

    # Ensure all weights are non-negative.
    g = args[0]
    for i in g.nodes():
        for j in g.nodes():
            if i != j:
                pre = assert_in_range(g.matrix[i][j], 0, len(g.nodes()) - 1)
                preconditions.append(pre)
    return preconditions


def model_to_input(model, size):
    matrix = []
    for i in range(size):
        row = []
        for j in range(size):
            row.append(0)
        matrix.append(row)
    g = WGraph(matrix)

    for var, value in model.items():
        i, j = re.match(r'g\[(\d+)\]\[(\d+)\]', var).groups()
        g.add_edge(int(i), int(j), int(value))

    for n in g.nodes():
        g.remove_edge(n, n)

    return g, 0


def dijkstra(g, source):
    distance = {}
    predecessor = {}

    vertices = g.nodes()
    for v in vertices:
        distance[v] = INF
        predecessor[v] = None

    distance[source] = 0

    while vertices:
        u = get_min(vertices, distance)
        vertices.remove(u)

        for v, w in g.neighbours(u):
            alt = distance[u] + w
            if alt < distance[v]:
                distance[v] = alt
                predecessor[v] = u

    return distance, predecessor


def get_min(vertices, distance):
    min_v = None
    for v in vertices:
        if min_v is None or distance[v] < distance[min_v]:
            min_v = v
    return min_v


if __name__ == '__main__':
    matrix = [
        [None, 3, None, 2, None, None, 6],
        [None, None, 6, None, 1, None, None],
        [None, None, None, None, None, 1, None],
        [None, 2, None, None, 3, None, None],
        [None, None, None, None, None, 4, None],
        [None, None, None, None, None, None, None],
        [None, None, None, None, None, 2, None],
    ]
    g = WGraph(matrix)
    print(dijkstra(g, 0))

    with stopwatch('wcetpp'):
        size = 3
        args = args_gen(size)
        kwargs = kwargs_gen(size)

        se = SymbolicExecutor()
        se.preconditions = preconditions_gen(size, args, kwargs)
        se.timeout = 10

        profiles = wcetpp(
            se,
            dijkstra,
            args,
            kwargs,
        )
    pp_wcet_profiles(profiles)

    # Non-deterministic policy.
    with stopwatch('analyze_wcet'):
        profiles = analyze_wcet(
            dijkstra,
            args_gen,
            kwargs_gen,
            preconditions_gen,
            min_training_size=1,
            max_training_size=3,
            timeout=10,
            target_size=4,
            history_size=2
        )
    pp_wcet_profiles(profiles)
