from complexity.structs import slist
from complexity.symex import SymbolicExecutor
from complexity.utils import pp_wcet_profiles
from complexity.utils import stopwatch
from complexity.executors.bruteforce_executor import analyze_wcet
from complexity.executors.bruteforce_executor import wcetpp


def args_gen(size):
    return slist('l', size),


def kwargs_gen(size):
    return {}


def preconditions_gen(size, args, kwargs):
    return []


def model_to_input(model, size):
    l = [0] * size
    for var, value in model.items():
        index = int(var.strip('l[]'))
        l[index] = int(value)
    return l,


def insertion_sort(l):
    for i in range(1, len(l)):
        j = i
        temp = l[j]
        while j > 0 and temp < l[j - 1]:
            l[j] = l[j - 1]
            j = j - 1
        l[j] = temp
    return l


if __name__ == '__main__':
    l = [5, 4, 3, 2, 1]
    print(insertion_sort(l))

    with stopwatch('wcetpp'):
        size = 5
        args = args_gen(size)
        kwargs = kwargs_gen(size)

        se = SymbolicExecutor()
        se.preconditions = preconditions_gen(size, args, kwargs)

        profiles = wcetpp(
            se,
            insertion_sort,
            args,
            kwargs
        )
    pp_wcet_profiles(profiles)

    with stopwatch('analyze_wcet'):
        profiles = analyze_wcet(
            insertion_sort,
            args_gen,
            kwargs_gen,
            preconditions_gen,
            min_training_size=0,
            max_training_size=5,
            timeout=5,
            target_size=30,
            history_size=1
        )
    pp_wcet_profiles(profiles)
