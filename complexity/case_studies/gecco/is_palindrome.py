from complexity.structs import slist
from complexity.symex import SymbolicExecutor
from complexity.utils import pp_wcet_profiles
from complexity.utils import stopwatch
from complexity.executors.bruteforce_executor import analyze_wcet
from complexity.executors.bruteforce_executor import wcetpp


def args_gen(size):
    return slist('l', size),


def kwargs_gen(size):
    return {}


def preconditions_gen(size, args, kwargs):
    return []


def model_to_input(model, size):
    l = [0] * size
    for var, value in model.items():
        index = int(var.strip('l[]'))
        l[index] = int(value)
    return l,


def is_palindrome(l):
    """Return `True` if the input list is palindrome.

    Note:
        Expected worst case: a palindrome list.
    """
    for i in range(len(l)):
        if l[i] != l[-(i+1)]:
            return False
    return True


if __name__ == '__main__':
    with stopwatch('wcetpp'):
        size = 15
        args = args_gen(size)
        kwargs = kwargs_gen(size)

        se = SymbolicExecutor()
        se.preconditions = preconditions_gen(size, args, kwargs)

        profiles = wcetpp(
            se,
            is_palindrome,
            args,
            kwargs,
        )
    pp_wcet_profiles(profiles)

    with stopwatch('analyze_wcet'):
        profiles = analyze_wcet(
            is_palindrome,
            args_gen,
            kwargs_gen,
            preconditions_gen,
            min_training_size=0,
            max_training_size=10,
            timeout=5,
            target_size=20,
            history_size=1,
        )
    pp_wcet_profiles(profiles)
