from complexity.structs import slist
from complexity.symex import SymbolicExecutor
from complexity.utils import pp_wcet_profiles
from complexity.utils import stopwatch
from complexity.executors.bruteforce_executor import analyze_wcet
from complexity.executors.bruteforce_executor import wcetpp


MEMORY_SIZE = 16


def args_gen(size):
    return slist('l', size),


def kwargs_gen(size):
    return {}


def preconditions_gen(size, args, kwargs):
    return []


def model_to_input(model, size):
    l = [0] * size
    for var, value in model.items():
        index = int(var.strip('l[]'))
        l[index] = int(value)
    return l,


def memory_fill(l):
    result = [0] * MEMORY_SIZE
    skipped = 0

    if len(l) > MEMORY_SIZE:
        counter = 0
        for i in range(len(l)):
            if l[i] == 0:
                skipped += 1
            else:
                if counter < MEMORY_SIZE:
                    result[counter] = l[i]
                counter += 1
    else:
        for i in range(len(l)):
            result[i] = l[i]

    return result, skipped


if __name__ == '__main__':
    l0 = list(range(32))
    print(memory_fill(l0))

    l1 = list(range(8))
    print(memory_fill(l1))

    with stopwatch('wcetpp'):
        size = 20
        args = args_gen(size)
        kwargs = kwargs_gen(size)

        se = SymbolicExecutor()
        se.preconditions = preconditions_gen(size, args, kwargs)
        se.timeout = 5

        profiles = wcetpp(
            se,
            memory_fill,
            args,
            kwargs
        )
    pp_wcet_profiles(profiles)

    with stopwatch('analyze_wcet'):
        profiles = analyze_wcet(
            memory_fill,
            args_gen,
            kwargs_gen,
            preconditions_gen,
            min_training_size=0,
            max_training_size=10,
            timeout=5,
            target_size=20,
            history_size=1,
        )
    pp_wcet_profiles(profiles)
