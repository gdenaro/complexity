from complexity.structs import slist
from complexity.symex import SymbolicExecutor
from complexity.utils import pp_wcet_profiles
from complexity.utils import stopwatch
from complexity.executors.bruteforce_executor import analyze_wcet
from complexity.executors.bruteforce_executor import wcetpp


def args_gen(size):
    return slist('l', size),


def kwargs_gen(size):
    return {}


def preconditions_gen(size, args, kwargs):
    return []


def model_to_input(model, size):
    l = [0] * size
    for var, value in model.items():
        index = int(var.strip('l[]'))
        l[index] = int(value)
    return l,


def merge_sort(l):
    if len(l) > 1:
        mid = len(l) // 2
        left = l[:mid]
        right = l[mid:]

        merge_sort(left)
        merge_sort(right)

        i = 0
        j = 0
        k = 0
        while i < len(left) and j < len(right):
            if left[i] < right[j]:
                l[k] = left[i]
                i += 1
            else:
                l[k] = right[j]
                j += 1
            k += 1

        while i < len(left):
            l[k] = left[i]
            i += 1
            k += 1

        while j < len(right):
            l[k] = right[j]
            j += 1
            k += 1


if __name__ == '__main__':
    with stopwatch('wcetpp'):
        size = 5
        args = args_gen(size)
        kwargs = kwargs_gen(size)

        se = SymbolicExecutor()
        se.preconditions = preconditions_gen(size, args, kwargs)

        profiles = wcetpp(
            se,
            merge_sort,
            args,
            kwargs
        )
    pp_wcet_profiles(profiles)

    with stopwatch('analyze_wcet'):
        profiles = analyze_wcet(
            merge_sort,
            args_gen,
            kwargs_gen,
            preconditions_gen,
            min_training_size=0,
            max_training_size=5,
            timeout=5,
            target_size=30,
            history_size=1
        )
    pp_wcet_profiles(profiles)
