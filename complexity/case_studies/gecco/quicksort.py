from complexity.structs import slist
from complexity.symex import SymbolicExecutor
from complexity.utils import pp_wcet_profiles
from complexity.utils import stopwatch
from complexity.executors.bruteforce_executor import analyze_wcet
from complexity.executors.bruteforce_executor import wcetpp


def args_gen(size):
    return slist('l', size),


def kwargs_gen(size):
    return {}


def preconditions_gen(size, args, kwargs):
    return []


def model_to_input(model, size):
    l = [0] * size
    for var, value in model.items():
        index = int(var.strip('l[]'))
        l[index] = int(value)
    return l,


def quicksort(l):
    _quicksort(l, 0, len(l) - 1)
    return l


def _quicksort(l, begin, end):
    if begin < end:
        pivot = partition(l, begin, end)
        _quicksort(l, begin, pivot - 1)
        _quicksort(l, pivot + 1, end)

    return l


def partition(l, begin, end):
    pivot = begin

    for i in range(begin + 1, end + 1):
        if l[i] <= l[begin]:
            pivot += 1
            l[i], l[pivot] = l[pivot], l[i]

    l[pivot], l[begin] = l[begin], l[pivot]

    return pivot


if __name__ == '__main__':
    l = [5, 4, 3, 2, 1, ]
    print(quicksort(l))

    with stopwatch('wcetpp'):
        size = 5
        args = args_gen(size)
        kwargs = kwargs_gen(size)

        se = SymbolicExecutor()
        se.preconditions = preconditions_gen(size, args, kwargs)

        profiles = wcetpp(
            se,
            quicksort,
            args,
            kwargs
        )
    pp_wcet_profiles(profiles)

    with stopwatch('analyze_wcet'):
        profiles = analyze_wcet(
            quicksort,
            args_gen,
            kwargs_gen,
            preconditions_gen,
            min_training_size=0,
            max_training_size=5,
            timeout=5,
            target_size=30,
            history_size=1
        )
    pp_wcet_profiles(profiles)
