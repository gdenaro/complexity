from complexity.executors.bruteforce_executor import SymbolicExecutor
from complexity.utils import pp_wcet_profiles
from complexity.utils import stopwatch
from complexity.executors.bruteforce_executor import analyze_wcet
from complexity.executors.bruteforce_executor import wcetpp

from itertools import permutations
import re

import z3


def args_gen(size):
    return [[z3.Int(f'l[{i}][0]'), z3.Int(f'l[{i}][1]')] for i in range(size)],


def kwargs_gen(size):
    return {}


def preconditions_gen(size, args, kwargs):
    return []


def model_to_input(model, size):
    l = [[0, 0] for _ in range(size)]
    for var, value in model.items():
        i, j = re.match(r'l\[(\d+)\]\[(\d+)\]', var).groups()
        l[int(i)][int(j)] = int(value)
    return l,


def traveling_salesman_problem(l):
    init_path = list(range(len(l)))
    min_length = calc_length(l, init_path)
    min_path = init_path

    for path in permutations(init_path):
        length = calc_length(l, path)
        if length < min_length:
            min_length = length
            min_path = path

    return min_path


def dist_manhattan(city_1, city_2):
    return abs(city_2[0] - city_1[0]) + abs(city_2[1] - city_1[1])


def calc_length(cities, path):
    length = 0

    for a, b in zip(path, path[1:] + path[:1]):
        length += dist_manhattan(cities[a], cities[b])

    return length


if __name__ == '__main__':
    cities = [
        (0, 0),
        (1, 1),
        (2, 2),
        (3, 3),
        (4, 4),
    ]
    print(traveling_salesman_problem(cities))

    with stopwatch('wcetpp'):
        size = 5
        args = args_gen(size)
        kwargs = kwargs_gen(size)

        se = SymbolicExecutor()
        se.preconditions = preconditions_gen(size, args, kwargs)
        se.timeout = 10

        profiles = wcetpp(
            se,
            traveling_salesman_problem,
            args,
            kwargs,
        )
    pp_wcet_profiles(profiles)

    with stopwatch('analyze_wcet'):
        profiles = analyze_wcet(
            traveling_salesman_problem,
            args_gen,
            kwargs_gen,
            preconditions_gen,
            min_training_size=1,
            max_training_size=5,
            timeout=5,
            target_size=20,
            history_size=1,
        )
    pp_wcet_profiles(profiles)
