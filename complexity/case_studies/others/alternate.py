from complexity.structs import slist
from complexity.symex import SymbolicExecutor
from complexity.utils import pp_wcet_profiles
from complexity.utils import stopwatch
from complexity.executors.bruteforce_executor import analyze_wcet
from complexity.executors.bruteforce_executor import wcetpp


def args_gen(size):
    return slist('l', size),


def kwargs_gen(size):
    return {}


def preconditions_gen(size, args, kwargs):
    return []


def count(n):
    for i in range(n):
        pass


def alternate(l):
    """Computes a lot if the input list alternates zero and non-zero numbers.

    Note:
        Expected worst case: a list of the form [0, non-zero, 0, non-zero, 0, ...].
    """
    for i, e in enumerate(l):
        if e == 0:
            count(1 - (i % 2))
        else:
            count(i % 2)


if __name__ == '__main__':
    with stopwatch('wcetpp'):
        size = 5
        args = args_gen(size)
        kwargs = kwargs_gen(size)

        se = SymbolicExecutor()
        se.preconditions = preconditions_gen(size, args, kwargs)
        se.timeout = 10

        profiles = wcetpp(
            se,
            alternate,
            args,
            kwargs
        )
    pp_wcet_profiles(profiles)

    with stopwatch('analyze_wcet'):
        profiles = analyze_wcet(
            alternate,
            args_gen,
            kwargs_gen,
            preconditions_gen,
            min_training_size=0,
            max_training_size=10,
            timeout=5,
            target_size=20,
            history_size=1,
        )
    pp_wcet_profiles(profiles)
