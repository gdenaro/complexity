from complexity.structs import swgraph
from complexity.symex import SymbolicExecutor
from complexity.utils import assert_in_range
from complexity.utils import pp_wcet_profiles
from complexity.utils import stopwatch
from complexity.executors.bruteforce_executor import analyze_wcet
from complexity.executors.bruteforce_executor import wcetpp


def args_gen(size):
    graph = swgraph('g', size)

    # Remove self-loops.
    for n in graph.nodes():
        graph.remove_edge(n, n)

    return graph, 0


def kwargs_gen(size):
    return {}


def preconditions_gen(size, args, kwargs):
    preconditions = []

    # Ensure all weights are non-negative.
    g = args[0]
    for i in g.nodes():
        for j in g.nodes():
            if i != j:
                pre = assert_in_range(g.matrix[i][j], 0, len(g.nodes()) - 1)
                preconditions.append(pre)
    return preconditions


def bellman_ford(g, source):
    """Calculate all shortest paths from source in g.

    Note:
        Expected worst case: unknown.
    """
    distance = {}
    predecessor = {}

    # First, initialize the dictionaries.
    vertices = g.nodes()
    for v in vertices:
        # At the beginning all vertices have infinite weight and no predecessor.
        distance[v] = 1 << 32
        predecessor[v] = None

    distance[source] = 0

    # Relax edges repeatedly.
    edges = g.edges()
    for _ in range(1, len(vertices) - 1):
        for u, v, w in edges:
            if distance[u] + w < distance[v]:
                distance[v] = distance[u] + w
                predecessor[v] = u

    return distance, predecessor


if __name__ == '__main__':
    with stopwatch('wcetpp'):
        size = 3
        args = args_gen(size)
        kwargs = kwargs_gen(size)

        se = SymbolicExecutor()
        se.preconditions = preconditions_gen(size, args, kwargs)
        se.timeout = 10

        profiles = wcetpp(
            se,
            bellman_ford,
            args,
            kwargs,
        )
    pp_wcet_profiles(profiles)

    # Non-deterministic policy.
    with stopwatch('analyze_wcet'):
        profiles = analyze_wcet(
            bellman_ford,
            args_gen,
            kwargs_gen,
            preconditions_gen,
            min_training_size=1,
            max_training_size=3,
            timeout=10,
            target_size=4,
            history_size=2
        )
    pp_wcet_profiles(profiles)
