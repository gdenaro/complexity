import z3

from complexity.structs import slist
from complexity.symex import SymbolicExecutor
from complexity.utils import assert_all_different
from complexity.utils import assert_all_positive
from complexity.utils import assert_sorted
from complexity.utils import pp_wcet_profiles
from complexity.utils import stopwatch
from complexity.executors.bruteforce_executor import analyze_wcet
from complexity.executors.bruteforce_executor import wcetpp


def args_gen(size):
    return slist('l', size), z3.Int('e')


def kwargs_gen(size):
    return {}


def preconditions_gen(size, args, kwargs):
    return [
        assert_all_positive(args[0]),
        assert_all_different(args[0]),
        assert_sorted(args[0])
    ]


def binsearch(l, e, lower, upper):
    if lower > upper:
        return False

    mid = (upper + lower) // 2
    if l[mid] == e:
        return True
    elif l[mid] > e:
        return binsearch(l, e, lower, mid-1)
    else:
        return binsearch(l, e, mid+1, upper)


def exp_search(l, e):
    if not l:
        return False

    bound = 1
    while bound < len(l) and l[bound] < e:
        bound <<= 1

    return binsearch(l, e, bound >> 1, min(bound, len(l)-1))


if __name__ == '__main__':
    with stopwatch('wcetpp'):
        size = 100
        args = args_gen(size)
        kwargs = kwargs_gen(size)

        se = SymbolicExecutor()
        se.preconditions = preconditions_gen(size, args, kwargs)
        se.timeout = 30

        profiles = wcetpp(
            se,
            exp_search,
            args,
            kwargs
        )
    pp_wcet_profiles(profiles)

    with stopwatch('analyze_wcet'):
        profiles = analyze_wcet(
            exp_search,
            args_gen,
            kwargs_gen,
            preconditions_gen,
            min_training_size=0,
            max_training_size=10,
            timeout=30,
            target_size=150,
            history_size=2,
        )
    pp_wcet_profiles(profiles)
