import logging
import random
import z3
import csv

from collections import namedtuple
from complexity.symex import RandomWalkSymbolicExecutor
from complexity.symex import SymbolicExecutor
from complexity.symex import PcClause
from complexity.symex import formula_slicing
from complexity.utils import conjuncts
from complexity.utils import is_inconsistent
from complexity.utils import minimize
from complexity.utils import negate
from complexity.utils import to_smt2
from complexity.utils import solver_assert
from complexity.utils import mk_and
from complexity.utils import mk_not
from complexity.utils import count_instructions
from complexity.utils import model_dict_to_string
from complexity.utils import model_to_dict
from complexity.executors.bruteforce_executor import wcetpp
from complexity.executors.bruteforce_executor import PathProfile
from functools import lru_cache
from itertools import chain
from math import ceil


from complexity.experimental.non_daemonic_pool import NonDaemonicPool


logger = logging.getLogger(__name__)


EVOLUTION_CSV_FIELDS = [
    'generation',
    'phase',
    'input',
    'cost',
]


Configuration = namedtuple('Configuration', [
    'module',
    'function',
    'size',
    'args_gen',
    'kwargs_gen',
    'preconditions_gen',
    'model_to_input',
    'timeout',
    'seed',
    'population_size',
    'generations',
    'crossover',
    'mutation_probability',
    'elitism_rate',
    'processors',
    'random_search',
    'evolution_csv',
])


def write_individuals_to_csv(csv_writer, generation, phase, individuals):
    for individual in individuals:
        csv_writer.writerow({
            'generation': generation,
            'phase': phase,
            'input': individual.constraint_set,
            'cost': individual.fitness,
        })


class Individual:
    def __init__(self, constraint_set, fitness):
        # Automatically minimize constraint sets to simplify single point crossover.
        self.constraint_set = constraint_set #minimize(constraint_set) #GIO
        self.fitness = fitness

    @classmethod
    def from_strings(cls, smt2_strings, fitness):
        """Return an individual with the given constraints and fitness.

        Args:
            smt2_strings (:iterable:string): An iterable of strings describing constraints in SMTLIB v2 format.
            fitness (int): The fitness of the individual.

        Returns:
            (Individual): An individual with the given constraints and fitness.
        """
        return Individual([z3.parse_smt2_string(s) for s in smt2_strings], fitness)

    def __eq__(self, other):
        return set(self.constraint_set) == set(other.constraint_set)

    def __hash__(self):
        return hash(tuple(self.constraint_set))

    def __reduce__(self):
        """Make individual picklable by serializing constraints to SMTLIB v2 format.

        Returns:
            (:tuple:(method, :list:string)): The method to deserialize the individual and a list of strings representing
                the constraints of this individual in SMTLIB v2 format.
        """
        constraint_set_as_strings = [to_smt2(constraint) for constraint in self.constraint_set]
        return Individual.from_strings, (constraint_set_as_strings, self.fitness)

    def __repr__(self):
        return f'Individual(fitness={self.fitness}, constraints={self.constraint_set})'


class HallOfFame:
    def __init__(self, n):
        self.n = n
        self.best_individuals = []

    def update(self, population):
        self.best_individuals.extend(population)
        self.best_individuals = list(set(self.best_individuals))
        self.best_individuals.sort(key=lambda individual: -individual.fitness)
        self.best_individuals = self.best_individuals[:self.n]

    def best_individual(self):
        return self.best_individuals[0]

    def __repr__(self):
        return '\n'.join(map(repr, self.best_individuals))


GAConfig = namedtuple('GAConfig', [
    'generations',
    'population_size',
    'crossover',
    'mutation_prob',
    'elite_ratio',
    'timeout',
    'pool_size',
    'seed',
    'random_search',
    'evolution_csv',
])


def pc_to_constraint_set(pc):
    """Transforms a path condition represented as a string to a constraint set."""
    formula = z3.parse_smt2_string(pc)
    formula = z3.simplify(formula)
    return conjuncts(formula)


random_individual = None
"""Global name of the function that generates random individuals, declared locally by the `ga_wcet_generator` function.

This global reference ensures that the function is picklable.
"""


single_point_crossover = None
"""Global name of the crossover function, declared locally by the `ga_wcet_generator` function.

This global reference ensures that the function is picklable.
"""


prefix_crossover = None
"""Global name of the crossover function, declared locally by the `ga_wcet_generator` function.

This global reference ensures that the function is picklable.
"""


exclude_crossover = None
"""Global name of the crossover function, declared locally by the `ga_wcet_generator` function.

This global reference ensures that the function is picklable.
"""


union_crossover = None
"""Global name of the crossover function, declared locally by the `ga_wcet_generator` function.

This global reference ensures that the function is picklable.
"""


mutation = None
"""Global name of the mutation function, declared locally by the `ga_wcet_generator` function.

This global reference ensures that the function is picklable.
"""


def ga_wcet_generator(
        f,
        args,
        kwargs,
        config):

    # This makes some inner functions defined as global functions instead.
    # This (rather nasty) trick makes them pickleable.
    global random_individual
    global single_point_crossover
    global prefix_crossover
    global exclude_crossover
    global union_crossover
    global mutation

    # Initialize the random number generator.
    rng = random.Random()
    rng.seed(config.seed)

    # Initialize the CSV writer.
    csv_writer = None
    if config.evolution_csv:
        csv_writer = csv.DictWriter(config.evolution_csv, EVOLUTION_CSV_FIELDS, quoting=csv.QUOTE_NONNUMERIC,
                                    lineterminator='\n')
        csv_writer.writeheader()

    def log_population(population):
        for i, individual in enumerate(sorted(population, key=lambda individual: -individual.fitness)):
            logger.debug(f'{i}) {individual}')

    def log_population_stats(population):
        fitnesses = [individual.fitness for individual in population]
        min_fitness = min(fitnesses)
        max_fitness = max(fitnesses)
        avg_fitness = sum(fitnesses) / len(fitnesses)
        logger.info(f'max: {max_fitness}, min: {min_fitness}, avg: {avg_fitness}')

    def constrained_wcetpp(preconditions):
        se = RandomWalkSymbolicExecutor(rng)
        se.timeout = config.timeout
        se.preconditions = preconditions
        return wcetpp(se, f, args, kwargs)

    def random_individual(random_seed):
        # Set the seed to a random number provided by the main process.
        rng.seed(random_seed)

        profiles = constrained_wcetpp([])
        profile = rng.choice(profiles)
        ind = Individual(pc_to_constraint_set(profile.pc), profile.cost)
        return ind

    @lru_cache(4096)
    def evaluate(constraint_set):
        profiles = constrained_wcetpp(constraint_set)
        profile = rng.choice(profiles)
        return Individual(pc_to_constraint_set(profile.pc), profile.cost)

    def local_search(individual):
        index = rng.randint(0, len(individual.constraint_set) - 1)
        remaining_attempts = config.population_size // 2
        print("Local search starts at index ", index)
        while remaining_attempts > 0:
            remaining_attempts -= 1

            current_constraints = list(individual.constraint_set)
            current_constraints[index] = mk_not(current_constraints[index])

            se = RandomWalkSymbolicExecutor(rng)
            #if individual.fitness > 150: #340:
            #    se.to_print = True
            se.preconditions = current_constraints
            profiles = wcetpp(se, f, args, kwargs)
            if profiles:
                profile = rng.choice(profiles)

                print("Local search at index ", index, ": ", individual.fitness, " --> ", profile.cost)
                if profile.cost > individual.fitness:
                    print("**Successful")
                    individual = Individual(pc_to_constraint_set(profile.pc), profile.cost)
                    if remaining_attempts < 5:
                        remaining_attempts = 5
                elif profile.cost == individual.fitness:
                    individual = Individual(pc_to_constraint_set(profile.pc), profile.cost)

            index = (index + 1) % len(individual.constraint_set)

        return individual


    def rank(population):
        ranking = []
        curr_rank = 0
        curr_fitness = 0
        for i in range(len(population)):
            if population[i].fitness > curr_fitness:
                curr_fitness = population[i].fitness
                curr_rank = i + 1
            ranking.append(curr_rank)
        return ranking

    def roulette_wheel_old(population):
        fitness_sum = sum(individual.fitness for individual in population)
        pick = rng.uniform(0, fitness_sum)
        current = 0
        for individual in population:
            current += individual.fitness
            if current > pick:
                return individual

    def roulette_wheel_deltas(population):
        fitnesses = [individual.fitness for individual in population]
        min_fitness = min(fitnesses)
        fitness_sum = sum(individual.fitness - min_fitness + 1 for individual in population)
        pick = rng.uniform(0, fitness_sum)
        current = 0
        for individual in population:
            current += individual.fitness - min_fitness + 1
            if current > pick:
                return individual

    def roulette_wheel(population):
        population = sorted(population, key=lambda individual: individual.fitness)
        ranking = rank(population)
        rank_sum = sum(rank for rank in ranking)
        pick = rng.uniform(0, rank_sum)
        current = 0
        for i in range(len(ranking)):
            current += ranking[i]
            if current > pick:
                return population[i]

    def selection(population, n):
        population = list(population)
        result = []
        for i in range(n):
            individual1 = roulette_wheel(population)
            population.remove(individual1)

            individual2 = roulette_wheel(population)
            population.append(individual1)

            result.append((individual1, individual2))
        return result

    def single_point_crossover(random_seed, parents):
        # Set the seed to a random number provided by the main process.
        rng.seed(random_seed)

        individual1, individual2 = parents

        constraints1 = individual1.constraint_set
        constraints2 = individual2.constraint_set

        cp1 = rng.randint(1, len(constraints1) - 1) if len(constraints1) > 1 else 0
        cp2 = rng.randint(1, len(constraints2) - 1) if len(constraints2) > 1 else 0

        def combine(constraints1, constraints2):
            result = list(constraints1)
            result_as_clauses = [PcClause(constraint, False) for constraint in result]
            for c in constraints2:
                slice = formula_slicing(result_as_clauses, c)
                if not slice:
                    result.append(c)
                    result_as_clauses.append(PcClause(c, False))
                elif slice[0] == True:
                    pass
                elif slice[0] == False:
                    pass
                else:
                    consistency_check = [clause.conditional for clause in slice]
                    consistency_check.append(c)
                    if not is_inconsistent(consistency_check):
                        result.append(c)
                        result_as_clauses.append(PcClause(c, False))
            return result

        child_constraints1 = combine(constraints1[:cp1], constraints2[cp2:])
        child_constraints2 = combine(constraints2[:cp2], constraints1[cp1:])

        child_constraints1 = mutation_bis(child_constraints1)
        child_constraints2 = mutation_bis(child_constraints2)

        child1 = evaluate(tuple(child_constraints1))
        child2 = evaluate(tuple(child_constraints2))
        return child1, child2

    def prefix_crossover(random_seed, parents):
        rng.seed(random_seed)

        parent1, parent2 = parents

        parent1_constraints = parent1.constraint_set
        parent2_constraints = parent2.constraint_set

        def split_constraints(constraints):
            if len(constraints) == 0:
                return [], [], []
            elif len(constraints) == 1:
                return constraints, [], []
            elif len(constraints) == 2:
                return constraints[0], constraints[1], []
            elif len(constraints) == 3:
                return constraints[0], constraints[1], constraints[2]
            if len(constraints) > 2:
                point1 = rng.randint(1, len(constraints) - 2)
                point2 = rng.randint(point1 + 1, len(constraints) - 1)
                return constraints[:point1], constraints[point1:point2], constraints[point2:]

        parent1_prefix, parent1_split1, parent1_split2 = split_constraints(parent1_constraints)
        parent2_prefix, parent2_split1, parent2_split2 = split_constraints(parent2_constraints)

        def combine(constraints1, constraints2):
            result = list(constraints1)
            for c in constraints2:
                result.append(c)
                if is_inconsistent(result):
                    result.pop()
            return result

        child1_contraints = combine(parent1_prefix, rng.choice((parent2_split1, parent2_split2)))
        child2_contraints = combine(parent2_prefix, rng.choice((parent1_split1, parent1_split2)))

        child1 = evaluate(tuple(child1_contraints))
        child2 = evaluate(tuple(child2_contraints))
        return child1, child2

    def exclude_crossover(random_seed, parents):
        rng.seed(random_seed)

        parent1, parent2 = parents

        parent1_constraints = parent1.constraint_set
        parent2_constraints = parent2.constraint_set

        def split_constraints(constraints):
            if len(constraints) == 0:
                return [], [], []
            elif len(constraints) == 1:
                return constraints, [], []
            elif len(constraints) == 2:
                return constraints[0], constraints[1], []
            elif len(constraints) == 3:
                return constraints[0], constraints[1], constraints[2]
            if len(constraints) > 2:
                point1 = rng.randint(1, len(constraints) - 2)
                point2 = rng.randint(point1 + 1, len(constraints) - 1)
                return constraints[:point1], constraints[point1:point2], constraints[point2:]

        parent1_split1, parent1_split2, parent1_split3 = split_constraints(parent1_constraints)
        parent2_split1, parent2_split2, parent2_split3 = split_constraints(parent2_constraints)

        def combine(constraints1, constraints2):
            result = list(constraints1)
            for c in constraints2:
                result.append(c)
                if is_inconsistent(result):
                    result.pop()
            return result

        options = (
            (parent1_split1, parent2_split1),
            (parent1_split2, parent2_split2),
            (parent1_split3, parent2_split3),
        )

        choices = rng.sample(options, k=2)
        child1_contraints = combine(choices[0][0], choices[1][1])
        child2_contraints = combine(choices[1][0], choices[0][1])

        child1 = evaluate(tuple(child1_contraints))
        child2 = evaluate(tuple(child2_contraints))
        return child1, child2

    def union_crossover(individual1, individual2):
        constraints1 = set(individual1.constraints)
        constraints2 = set(individual2.constraints)
        all_constraints = constraints1 | constraints2

        child_constraints1 = []
        child_constraints2 = []
        while all_constraints:
            c = all_constraints.pop()
            not_c = negate(c)
            if not_c in all_constraints:
                all_constraints.remove(not_c)
                genes = [c, not_c]
                config.random.shuffle(genes)
                child_constraints1.append(genes[0])
                child_constraints2.append(genes[1])
            else:
                child_constraints1.append(c)
                child_constraints2.append(c)

        child1 = evaluate(tuple(child_constraints1))
        child2 = evaluate(tuple(child_constraints2))
        return child1, child2

    def delete_mutation(individual, ratio):
        nr_targets = ceil(ratio * len(individual.constraint_set))

        # Remove some random constraints.
        child_constraints = list(individual.constraint_set)
        for _ in range(nr_targets):
            index = rng.randint(0, len(child_constraints) - 1)
            del child_constraints[index]

        child = evaluate(tuple(child_constraints))
        return child

    def negate_mutation(individual, ratio):
        nr_constraints = len(individual.constraint_set)
        nr_targets = ceil(ratio * nr_constraints)

        # Negate some random constraints.
        child_constraints = list(individual.constraint_set)
        for index in rng.sample(range(nr_constraints), nr_targets):
            child_constraints[index] = negate(child_constraints[index])
            # If the negated constraint makes the constraint set inconsistent, revert.
            if is_inconsistent(child_constraints):
                child_constraints[index] = negate(child_constraints[index])

        child = evaluate(tuple(child_constraints))
        return child

    def delete_mutation_bis(child_constraints, ratio):
        nr_targets = ceil(ratio * len(child_constraints))

        # Remove some random constraints.
        for _ in range(nr_targets):
            index = rng.randint(0, len(child_constraints) - 1)
            del child_constraints[index]
            if rng.choice([True, False]):
                break

        return child_constraints

    def mutation_bis(child_constraints):
        if rng.random() < config.mutation_prob:
            return delete_mutation_bis(child_constraints, ratio=0.1)
        else:
            return child_constraints

    def mutation(random_seed, individual):
        # Set the seed to a random number provided by the main process.
        rng.seed(random_seed)

        if rng.random() < config.mutation_prob:
            if rng.choice([True, False]):
                return delete_mutation(individual, ratio=0.1)
            else:
                return negate_mutation(individual, ratio=0.1)
        return individual

    def elitism(population, n):
        population = sorted(population, key=lambda individual: -individual.fitness)
        return population[:n], population[n:]

    def survival_selection(population, n):
        population = list(population)
        result = []
        for i in range(n):
            individual = roulette_wheel(population)
            population.remove(individual)
            result.append(individual)
        return result

    elite_size = ceil(config.elite_ratio * config.population_size)
    hof = HallOfFame(5)

    if config.random_search:
        # It executes the same number of evaluations of the equivalent genetic algorithm execution.
        random_search_population_size = estimate_fitness_evaluations(config)
        logger.debug('generating and evaluating the random search population')
        random_seeds = [rng.getrandbits(32) for _ in range(random_search_population_size)]
        with NonDaemonicPool(config.pool_size) as p:
            population = p.map(random_individual, random_seeds)
        hof.update(population)
        log_population(population)
        logger.debug('hall of fame:')
        logger.debug(hof)

    else:  # Genetic Algorithm execution.
        # Generate initial population.
        logger.debug('generating initial population')
        random_seeds = [rng.getrandbits(32) for _ in range(config.population_size)]
        with NonDaemonicPool(config.pool_size) as p:
            population = p.map(random_individual, random_seeds)
        hof.update(population)
        log_population(population)

        logger.debug('hall of fame:')
        logger.debug(hof)

        for g in range(config.generations):
            logger.info(f'generation {g}:')

            if g % 5 == 0 and g > 0:
                best = elite[0]
                optimized_best = local_search(best)
                if optimized_best.fitness > best.fitness:
                    population.remove(best)
                    population.append(optimized_best)
                logger.debug('local search stats:')
                log_population_stats(population)

            if config.evolution_csv:
                write_individuals_to_csv(csv_writer, g, 'fitness', population)
                config.evolution_csv.flush()

            # Notice: selection generates a list of pairs using roulette wheel. Never generate pairs such as (a, a).
            parents = selection(population, n=(config.population_size // 2))
            print("parents")
            for pair in parents:
                print("* ", pair[0].fitness, " - ", pair[1].fitness)
            if config.evolution_csv:
                parents_population = []
                for pair in parents:
                    for individual in pair:
                        parents_population.append(individual)
                write_individuals_to_csv(csv_writer, g, 'selection', parents_population)
                config.evolution_csv.flush()

            random_seeds = [rng.getrandbits(32) for _ in parents]
            with NonDaemonicPool(config.pool_size) as p:
                if config.crossover == 'singlepoint':
                    crossover_function = single_point_crossover
                elif config.crossover == 'prefix':
                    crossover_function = prefix_crossover
                elif config.crossover == 'exclude':
                    crossover_function = exclude_crossover
                elif config.crossover == 'union':
                    crossover_function = union_crossover
                offspring = list(chain.from_iterable(p.starmap(crossover_function, zip(random_seeds, parents))))
            hof.update(offspring)

            logger.debug('offspring after crossover and mutation:')
            #log_population(offspring)
            log_population_stats(offspring)

            if config.evolution_csv:
                write_individuals_to_csv(csv_writer, g, 'crossover', offspring)
                config.evolution_csv.flush()

            #random_seeds = [rng.getrandbits(32) for _ in offspring]
            #with NonDaemonicPool(config.pool_size) as p:
            #    offspring = p.starmap(mutation, zip(random_seeds, offspring))
            #hof.update(offspring)

            #logger.debug('offspring after mutation:')
            ##log_population(offspring)
            #print (len(offspring))
            #log_population_stats(offspring)

            #if config.evolution_csv:
            #    write_individuals_to_csv(csv_writer, g, 'mutation', offspring)
            #    config.evolution_csv.flush()

            #elite_size = config.population_size
            elite, population = elitism(population + offspring, n=elite_size)
            population = survival_selection(population, n=(config.population_size - elite_size))
            population.extend(elite)
            print("best: ", elite[0])

            logger.debug('generation summary:')
            #log_population(population)
            log_population_stats(population)

            logger.debug('hall of fame:')
            #logger.debug(hof)

            if config.evolution_csv:
                write_individuals_to_csv(csv_writer, g, 'survival_selection', population)
                config.evolution_csv.flush()

    if config.evolution_csv:
        write_individuals_to_csv(csv_writer, config.generations - 1, 'result', population)
        config.evolution_csv.flush()

    best_individual = hof.best_individual()
    logger.info('best individual: %s', best_individual)

    solver = z3.Solver()
    solver.push()
    pc = list(best_individual.constraint_set)
    solver_assert(solver, pc)
    status = solver.check()
    solver.pop()

    best_profile = PathProfile([], best_individual.fitness, pc, model_to_dict(solver.model()), True)
    return [best_profile] #constrained_wcetpp(best_individual.constraint_set)


def estimate_fitness_evaluations(config):
    """Returns the estimated number of fitness evaluations to run `ga_wcet_generator` with the given configuration.

    Returns:
        (int): The estimated number of fitness evaluations to run `ga_wcet_generator` with the given configuration.
    """
    g = config.generations
    p = config.population_size
    mp = config.mutation_prob
    return int(ceil(p*((mp+1)*g + 1)))


def estimate_seconds(config):
    """Returns the estimated number of seconds to run `ga_wcet_generator` with the given configuration.

    Returns:
        (float): The estimated number of seconds to run `ga_wcet_generator` with the given configuration.
    """
    return estimate_fitness_evaluations(config) * config.timeout


def estimate_hours(config):
    """Returns the estimated number of hours to run `ga_wcet_generator` with the given configuration.

    Returns:
        (float): The estimated number of hours to run `ga_wcet_generator` with the given configuration.
    """
    return estimate_seconds(config) / 3600


def run(configuration):
    import importlib

    from complexity.utils import pp_wcet_profiles

    module_ = importlib.import_module(configuration.module)

    args = getattr(module_, configuration.args_gen)(configuration.size)
    kwargs = getattr(module_, configuration.kwargs_gen)(configuration.size)

    # Set the logger to DEBUG level to capture intermediate output.
    formatter = logging.Formatter(fmt='[%(asctime)s] %(message)s', datefmt='%m/%d/%Y %H:%M:%S')
    handler = logging.StreamHandler()
    handler.setFormatter(formatter)

    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)

    ga_configuration = GAConfig(
        generations=configuration.generations,
        population_size=configuration.population_size,
        crossover=configuration.crossover,
        mutation_prob=configuration.mutation_probability,
        elite_ratio=configuration.elitism_rate,
        timeout=configuration.timeout,
        pool_size=configuration.processors,
        seed=configuration.seed,
        random_search=configuration.random_search,
        evolution_csv=configuration.evolution_csv,
    )

    logger.info(f'configuration: {configuration}')
    logger.info(f'max estimated time: {estimate_hours(ga_configuration):.2f} hours')
    logger.info(f'max estimated fitness evaluations: {estimate_fitness_evaluations(ga_configuration)}')

    func = getattr(module_, configuration.function)

    profiles = ga_wcet_generator(
        func,
        args,
        kwargs,
        ga_configuration,
    )
    pp_wcet_profiles(profiles)

    model_to_input = getattr(module_, configuration.model_to_input)
    worst_case_model = model_dict_to_string(profiles[0].model)
    worst_case_input = model_to_input(profiles[0].model, configuration.size)
    worst_case_cost = count_instructions(func, *worst_case_input)
    return worst_case_model, worst_case_input, worst_case_cost
