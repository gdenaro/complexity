import logging

from collections import namedtuple

from complexity.executors.bruteforce_executor import analyze_wcet
from complexity.utils import count_instructions
from complexity.utils import model_dict_to_string


logger = logging.getLogger(__name__)


Configuration = namedtuple('Configuration', [
    'module',
    'function',
    'size',
    'args_gen',
    'kwargs_gen',
    'preconditions_gen',
    'model_to_input',
    'timeout',
    'min_training_size',
    'max_training_size',
    'history_size',
])


def run(configuration):
    import importlib

    from complexity.utils import pp_wcet_profiles

    module_ = importlib.import_module(configuration.module)

    formatter = logging.Formatter(fmt='[%(asctime)s] %(message)s', datefmt='%m/%d/%Y %H:%M:%S')
    handler = logging.StreamHandler()
    handler.setFormatter(formatter)

    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)

    logger.info(f'configuration: {configuration}')

    func = getattr(module_, configuration.function)

    profiles = analyze_wcet(
        func,
        getattr(module_, configuration.args_gen),
        getattr(module_, configuration.kwargs_gen),
        getattr(module_, configuration.preconditions_gen),
        min_training_size=configuration.min_training_size,
        max_training_size=configuration.max_training_size,
        timeout=configuration.timeout,
        target_size=configuration.size,
        history_size=configuration.history_size,
    )
    pp_wcet_profiles(profiles)

    model_to_input = getattr(module_, configuration.model_to_input)
    worst_case_model = model_dict_to_string(profiles[0].model)
    worst_case_input = model_to_input(profiles[0].model, configuration.size)
    worst_case_cost = count_instructions(func, *worst_case_input)
    return worst_case_model, worst_case_input, worst_case_cost
