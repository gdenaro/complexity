import builtins
import logging
import multiprocessing
import os
import signal
import traceback
import z3

from collections import defaultdict
from complexity.sbuiltins import srange
from complexity.sbuiltins import sabs
from complexity.tracer import untraceable
from complexity.utils import callerframe
from complexity.utils import mk_and
from complexity.utils import mk_not
from complexity.utils import override
from complexity.utils import patch
from complexity.utils import solver_assert
from complexity.utils import unpatch
from contextlib import contextmanager
from enum import auto
from enum import Enum


PRINT_TRACEBACK = False
"""If `True`, `SymbolicExecutor.run` prints captured exceptions tracebacks."""

logger = logging.getLogger(__name__)
"""The logger for this module."""


class Condition:
    """A condition in a program."""
    def __init__(self, f, lineno):
        """Create a new condition.

        Args:
            f (str): The name of a function.
            lineno (int): A line number in the function `f`.
        """
        self.f = f
        self.lineno = lineno

    def __hash__(self):
        """Return the hash value of this condition.

        Returns:
            (int): The hash value of this condition.
        """
        return hash((self.f, self.lineno))

    def __eq__(self, other):
        """Compare this condition with an object for equality.

        Args:
            other (object): The object to compare to this condition.

        Returns:
            (bool): `True` if `object` is also a condition referencing the same function and line number as this one.
        """
        if not isinstance(other, Condition):
            return False
        return self.f == other.f and self.lineno == other.lineno

    def __repr__(self):
        """Return a string representing this condition.

        Returns:
            (str): A string representing this condition.
        """
        return f'{self.f!r}:{self.lineno!r}'


class Decision:
    """A decision taken at a condition."""
    def __init__(self, condition, chosen_branch):
        """Create a new decision.

        Args:
            condition (Condition): The condition.
            chosen_branch (bool): The branch chosen at the given condition.
        """
        self.condition = condition
        self.chosen_branch = chosen_branch

    def __hash__(self):
        """Return the hash value of this decision.

        Returns:
            (int): The hash value of this decision.
        """
        return hash((self.condition, self.chosen_branch))

    def __eq__(self, other):
        """Compare this decision with an object for equality.

        Args:
            other (object): The object to compare to this condition.

        Returns:
            (bool): `True` if `object` is also a decision referencing the same condition and branch choice as this one.
        """
        if not isinstance(other, Decision):
            return False
        return self.condition == other.condition and self.chosen_branch == other.chosen_branch

    def __repr__(self):
        """Return a string representing this decision.

        Returns:
            (str): A string representing this decision.
        """
        choice = 'FT'[self.chosen_branch]
        return f'{choice!r}@[{self.condition!r}]'


class Path:
    """A list of decisions describing the branches taken at conditional statements."""

    def __init__(self, decisions=None):
        """Create a new path.

        Args:
            decisions (:list:Decision, optional): The list of decisions to initialize the path.
        """
        self.decisions = decisions or []

    def append(self, decision):
        """Append a decision to this path.

        Args:
            decision (Decision): The decision.
        """
        self.decisions.append(decision)

    def __repr__(self):
        """Return a string representing this path.

        Returns:
            (str): A string representing this path.
        """
        return '.'.join(map(repr, self.decisions))


class PcClause:
    """A clause of a path condition, which maintains the list of referred variables (to support slicing)."""

    def __init__(self, bool_ref, is_precondition):
        """Create a clause.

        Args:
            bool_ref (z3.BoolRef): The symbolic conditional.
            is_precondition (boolean): marker for initially assumed clauses (preconditions).
        """
        self.conditional = bool_ref
        self.is_precondition = is_precondition
        self.maybe_redundant = is_precondition # False, if this clause has proven impact on the pathcondition
        self.vars = set()
        self.get_variables = self.parse_variables


    def parse_variables(self):
        """Parses the variables in the clause. Invoked at the first call to self.get_variables().
        """
        self.get_variables = self.get_cached_variables

        if self.vars:
            return self.get_variables()

        worklist = [self.conditional]
        while worklist:
            child = worklist.pop()
            if z3.is_var(child):
                self.vars.add(child.__repr__())
            elif z3.is_int_value(child):
                pass
            elif z3.is_expr(child):
                children = child.children()
                if children:
                    worklist.extend(children)
                else:
                    self.vars.add(child.__repr__())

        #print(self.conditional, " - vars: ", self.vars)
        return self.vars

    def get_cached_variables(self):
        """Get the already parsed variables in the clause. Invoked when calling self.get_variables() but the first call.
        """
        #print(self.conditional, " - chached vars: ", self.vars)
        return self.vars

    def slice_vars(self, curr_slice_vars):
        """Returns the variables to be considered when adding this clause to a slice.

        Args:
            curr_slice_vars (set): The variables in the current slice.

        Returns:
            (set): the variables to be considered when adding this clause to a slice;
                empty, if this clause can be sliced away.
        """

        vars = self.get_variables() # parse vars if needed (not yet parsed)
        if curr_slice_vars & vars:
            return curr_slice_vars | vars
        else:
            return {}

    def __repr__(self):
        """Return a string representing this clause.

        Returns:
            (str): A string representing this clause.
        """
        return self.conditional.__repr__()

class Pathcondition:
    """A list of clauses describing the execution condition of the current path."""

    def __init__(self):
        """Create a new pathcondition."""
        self.clauses = []

    def append(self, clause):
        """Append a decision to this path.

        Args:
            clause (PcClause): The clause to be added.
        """
        self.clauses.append(clause)

    def get_constraints(self):
        """Return the list of constraints the corresponds to the clauses of this pathcondition.

        Returns:
            (list(z3.BoolRef): the list of constraints the corresponds to the clauses of this pathcondition.
        """
        bool_refs = []
        for clause in self.clauses:
            bool_refs.append(clause.conditional)
        return bool_refs

    def __repr__(self):
        """Return a string representing this pathcondition.

        Returns:
            (str): A string representing this pathcondition.
        """
        return '.'.join(map(repr, self.clauses))


class DeadEnd(Exception):
    """Exception raised to notify that a path has reached a dead end.

    Note:
        A path has reached a dead end if:

        1. its corresponding path condition is unsatisfiable, or
        2. the `on_branch` function of `SymbolicExecutor` returns an empty tuple.
    """
    pass


@contextmanager
def patch_environ(explore):
    """Monkey patch methods and functions to create a symbolic execution context.

    Args:
        explore (function): The function called on the evaluation of symbolic conditions.
    """
    patch(z3.BoolRef, '__bool__', explore)
    patch(z3.BoolRef, '__nonzero__', explore)
    patch(builtins, 'abs', sabs)
    patch(builtins, 'range', srange)

    try:
        yield
    finally:
        unpatch(z3.BoolRef, '__bool__')
        unpatch(z3.BoolRef, '__nonzero__')
        unpatch(builtins, 'abs')
        unpatch(builtins, 'range')


class Event(Enum):
    """A symbolic execution event.

    Note:
        This enum is used to register handlers for a `SymbolicExecutor`, which are called when specific events trigger.
    """
    ON_BEFORE_BRANCH = auto()
    ON_AFTER_BRANCH = auto()
    ON_BEFORE_FORK = auto()
    ON_AFTER_FORK = auto()
    ON_BEFORE_CHECK = auto()
    ON_AFTER_CHECK = auto()
    ON_RETURN = auto()
    ON_EXCEPT = auto()
    ON_DEAD_END = auto()
    ON_EXIT = auto()
    ON_TIMEOUT = auto()

def formula_slicing(current_clauses, bool_ref):
    def trivial_equivalence(bool_ref1, bool_ref2):
        return bool_ref1.__repr__() == bool_ref2.__repr__()

    def trivial_contradiction(bool_ref1, bool_ref2):
        # assumed precondition: !trivial_equivalence(bool_ref1, bool_ref2)
        if (z3.is_not(bool_ref1)):
            bool_ref1 = bool_ref1.children()[0]
        if (z3.is_not(bool_ref2)):
            bool_ref2 = bool_ref2.children()[0]
        return bool_ref1.__repr__() == bool_ref2.__repr__()

    slice = []
    branch_clause = PcClause(bool_ref, False)
    slice_vars = branch_clause.get_variables()

    # print("slicing ", curr_clause, "(vars", slice_vars, ") with ", pc, " - len=", len(pc))

    next_pass = []
    next_pass.extend(current_clauses)
    fix_point = False
    while not fix_point:
        fix_point = True  # set to False below if another pass is needed
        traversed_clauses = 0
        worklist = []
        worklist.extend(next_pass)
        next_pass = []
        for pc_clause in worklist:
            traversed_clauses += 1
            slice_vars_if_some_shared = pc_clause.slice_vars(slice_vars)
            if not slice_vars_if_some_shared:
                next_pass.append(pc_clause)
            else:
                if trivial_equivalence(bool_ref, pc_clause.conditional):
                    return [True, pc_clause]
                elif trivial_contradiction(bool_ref, pc_clause.conditional):
                    return [False, pc_clause]
                else:
                    slice_vars = slice_vars_if_some_shared
                    slice.append(pc_clause)
                    if next_pass:
                        fix_point = False

    return slice

class SymbolicExecutor:
    """
    A symbolic executor.

    Note:
        It tracks:

            1. the current execution path,
            2. the current path condition, and
            3. the model of the last satisfiable path condition.

        It can be configured with:

            1. a timeout for the symbolic execution of target callables,
            2. a list of preconditions, and
            3. a set of handlers called when specific events trigger.
    """

    def __init__(self):
        """Create a new symbolic executor."""

        self.path = Path()
        self.solver = z3.Solver()
        self.model = None

        self.timeout = None
        self.preconditions = []
        self.handlers = defaultdict(list)

        self.pc = Pathcondition()
        self.to_print = False

    def pc_to_constraint_list(self):
        """ Returns the path condition as a list of z3.BoolRef constraints.

        Returns:
            (list(z3.BoolRef)): the path condition as a list of z3.BoolRef constraints.
        """
        # get the constraints
        bool_refs = self.pc.get_constraints()

        # remove the preconditions
        bool_refs = bool_refs[len(self.preconditions):]

        return bool_refs

    def reset(self):
        """Reset this symbolic executor.

        Note:
            The symbolic executor timeout, its preconditions and its handlers are NOT reset.
        """
        self.path = Path()
        self.solver = z3.Solver()
        self.model = None

    def _log(self, msg, level=logging.INFO):
        """Log a message with the given level.

        Note:
            The log includes the current path and the caller pid.

        Args:
            msg (str): The message.
            level (int): The level of the log.
        """
        pid = os.getpid()
        path = self.path
        logger.log(level, '[%d][%r] %s', pid, path, msg)

    def ensure(self, bool_ref):
        """Add a boolean expression to the current path condition.

        Args:
            bool_ref (z3.BoolRef): The boolean expression.
        """
        solver_assert(self.solver, [bool_ref])

    def _notify_handlers(self, event, *args, **kwargs):
        """Call all handlers registered for the given event.

        Args:
            *args (:list:object): The positional parameters passed to the handlers.
            **kwargs (:dict:object): The keyword parameters passed to the handlers.
        """
        for handler in self.handlers[event]:
            handler(self, *args, **kwargs)

    def add_handler(self, event, handler):
        """Add a handler for the given event.

        Args:
            event (Event): The event on which the given handler will be called.
            handler (function): The handler.
        """
        self.handlers[event].append(handler)

    def remove_handler(self, event, handler):
        """Remove a handler for the given event.

        Args:
            event (Event): The event of the handler.
            handler (function): The handler.
        """
        self.handlers[event].remove(handler)

    def on_branch(self, condition, bool_ref):
        """Return a tuple describing the branches of the current condition to explore.

        Note:
            This function is called by `SymbolicExecutor._explore`.

            The possible return values are:

                1. (): no branch will be explored,
                2. (True): only the `True` branch will be explored,
                3. (False): only the `False` branch will be explored,
                4. (True, False): both the `True` and `False` branches will be explored, in this order,
                5. (False, True): both the `False` and `True` branches will be explored, in this order.

        Args:
            condition (Condition): The current condition.
            bool_ref (z3.BoolRef): The boolean expression corresponding to the current condition.

        Returns:
            (tuple): A tuple of at most two boolean values describing the branches to explore, each paired
                with the PcClause to be conjoined to the path condition when the branch is explored.
                The PcClause of a branch can be None, meaning that nothing new must be assumed.
        """
        relevant_pc_slice = formula_slicing(self.pc.clauses, bool_ref)
        if self.to_print:
            print("Evaluating: ", bool_ref, " - pc slice is ", relevant_pc_slice)
        if not relevant_pc_slice:
            # both branches are reachable, return without calling Z3
            # TODO: Fix: This holds only if the condition is linear in some referred variables
            return {True: PcClause(bool_ref, False), False: PcClause(mk_not(bool_ref), False)}
        elif relevant_pc_slice[0] == True:
            clause = relevant_pc_slice[1]
            if clause.is_precondition:
                self.pc.append(PcClause(clause.conditional, False))
                clause.is_precondition = False
                if self.to_print:
                    print("assuming pre ", clause.conditional, "for ", bool_ref, "in ", self.pc)
            return {True: None}
        elif relevant_pc_slice[0] == False:
            clause = relevant_pc_slice[1]
            if clause.is_precondition:
                self.pc.append(PcClause(clause.conditional, False))
                clause.is_precondition = False
                if self.to_print:
                    print("assuming pre ", clause.conditional, "for ", bool_ref, "in ", self.pc)
            return {False: None}

        reachable_branches = {}
        for branch in [True, False]:

            # Build the boolean expression corresponding to the target condition branch.
            c = bool_ref
            if not branch:
                c = mk_not(c)

            # Assume the resulting boolean expression.
            self._log(f'assume: {c}')

            # Solve the feasibility condition of this path.
            if not relevant_pc_slice:
                branch_feasibility_condition = c
            else:
                relevant_conditions = [clause.conditional for clause in relevant_pc_slice]
                relevant_conditions.append(c)
                branch_feasibility_condition = mk_and(relevant_conditions)

            self.solver.push()
            self.ensure(branch_feasibility_condition)
            self._notify_handlers(Event.ON_BEFORE_CHECK)
            self._log('solve path condition')
            status = self.solver.check()
            self._notify_handlers(Event.ON_AFTER_CHECK, status)
            self.solver.pop()

            if status == z3.sat:
                self._log('reachable')
                reachable_branches[branch] = PcClause(c, False)
            else: # assume the preconditions that impact this decision
                if self.to_print:
                    print("infeasible ", branch_feasibility_condition)

                done = False

                # first check if the clauses in the current pathconditon are already sufficient
                already_assumed = [cl.conditional for cl in relevant_pc_slice if not cl.is_precondition]
                already_assumed.append(c)
                already_assumed_z3 = mk_and(already_assumed)
                if already_assumed:
                    self.solver.push()
                    self.ensure(already_assumed_z3)
                    status = self.solver.check()
                    self.solver.pop()

                    if status == z3.unsat: # assume assume not(c), since the necessary_pre are insufficient
                        done = True
                        if self.to_print:
                            print("pc already sufficient with ", already_assumed, " for ", bool_ref, " in pc = ", self.pc)

                # otherwise, assume the single precondition, if any, that is sufficient for deciding this condition
                notyet_assumed = [cl for cl in relevant_pc_slice if cl.is_precondition]
                if not done and notyet_assumed:
                    for clause in notyet_assumed:
                        to_check = mk_and([already_assumed_z3, clause.conditional])
                        self.solver.push()
                        self.ensure(to_check)
                        status = self.solver.check()
                        self.solver.pop()

                        if status == z3.unsat:
                            done = True
                            self.pc.append(PcClause(clause.conditional, False))
                            clause.is_precondition = False
                            if self.to_print:
                                print("assuming sufficient pre ", clause.conditional, " for ", bool_ref, " in pc = ", self.pc)
                            break

                # Otherwise, conservatively assume all preconditions
                # however, if there are too many precondtions, better assuming not(current c)
                if not done and notyet_assumed:
                    if len(notyet_assumed) <= .2 * len(self.preconditions):
                        for clause in notyet_assumed:
                            self.pc.append(PcClause(clause.conditional, False))
                            clause.is_precondition = False
                        if self.to_print:
                            print("assumed all pre ", notyet_assumed, " for ", bool_ref, " in pc = ", self.pc)
                    elif branch:
                        self.pc.append(PcClause(mk_not(bool_ref), False))
                        if self.to_print:
                            print("assuming current to subsume unnecessary pre ", mk_not(bool_ref), " in ", self.pc)
                    else:
                        self.pc.append(PcClause(bool_ref, False))
                        if self.to_print:
                            print("assuming current to subsume unnecessary pre ", bool_ref, " in ", self.pc)

        if len(reachable_branches) == 1:
            # The pathcondition already entails the branch_clause
            reachable_branches = {key: None for key, value in reachable_branches.items()}

        return reachable_branches

    def handle_path(self, condition, branch):
        """
        Override this method if something has to handles for self.path.

        Args:
            condition (Condition): The current branch condition.
            branch (boolean): The branch side being explored.
        """
        #self.path.append(Decision(condition, branch))

    @untraceable
    def _explore(self, bool_ref):
        """Replacement method for the `__bool__` and `__nonzero__` methods of `z3.BoolRef`.

        This method forks the execution of the current process according to the result of `on_branch`
        and enables the symbolic execution of both branches of the current conditional.

        Args:
            bool_ref (z3.BoolRef): The boolean expression.
        """

        # Extract the frame corresponding to the current condition in the function under analysis.
        frame = callerframe(4)
        condition = Condition(frame.function, frame.lineno)

        # Prevent reference cycles.
        del frame

        self._notify_handlers(Event.ON_BEFORE_BRANCH)

        branches = self.on_branch(condition, bool_ref)
        if not branches:
            self._log('explore no branch')
            raise DeadEnd
        elif len(branches) == 1:
            branch, branch_clause = branches.popitem()
            self._log(f'explore one branch: {branch}')
        else:
            self._log('explore both branches')
            self._notify_handlers(Event.ON_BEFORE_FORK)
            pid = os.fork()
            if pid:
                os.waitpid(pid, 0)
                branch = False
            else:
                branch = True
            self._notify_handlers(Event.ON_AFTER_FORK, branch)
            branch_clause = branches.get(branch)

        # handle self.path
        self.handle_path(condition, branch)

        # update the pathcondition
        if branch_clause:
            self.pc.append(branch_clause)
            if self.to_print:
                print("assuming ", branch_clause.conditional, "for ", bool_ref, "in ", self.pc)

        self._notify_handlers(Event.ON_AFTER_BRANCH)

        return branch

    def run(self, f, *args, **kwargs):
        """Run the given function on the given (possibly symbolic) arguments.

        Args:
            f (function): The target callable to run.
            *args (:list:object): The positional parameters passed to `f`.
            **kwargs (:dict:object): The keyword parameters passed to `f`.
        """
        self._log(f'running callable {f.__name__}')

        # Event variable used to notify the main thread that the runner has become the leader of its process group.
        pgroup_set = multiprocessing.Event()

        def runner():
            # Notify the main thread that this runner has become the leader of its process group.
            os.setsid()
            pgroup_set.set()

            with patch_environ(lambda bool_ref: self._explore(bool_ref)):
                for precond in self.preconditions:
                    self.pc.append(PcClause(precond, True))

                if self.to_print:
                    print("pre is: ", self.pc)

                try:
                    output = f(*args, **kwargs)
                    self._log(f'output: {output}')
                    self._notify_handlers(Event.ON_RETURN, output)
                except DeadEnd as e:
                    self._log('dead end')
                    self._notify_handlers(Event.ON_DEAD_END)
                except Exception as e:
                    self._log(f'exception: {e}')
                    self._notify_handlers(Event.ON_EXCEPT, e)
                    if PRINT_TRACEBACK:
                        traceback.print_exc()
                finally:
                    self._log('exit')
                    self._notify_handlers(Event.ON_EXIT)
                    os._exit(os.EX_OK)

        # Run `runner` as a separate process.

        p = multiprocessing.Process(target=runner)
        p.start()

        # Wait for `runner` to become the leader of its process group.
        pgroup_set.wait()

        # Wait for at most `timeout` seconds the termination of `runner`.
        p.join(timeout=self.timeout)

        # If `runner` is still alive, kill it and all of its children by referencing its process group.
        if p.is_alive():
            self._notify_handlers(Event.ON_TIMEOUT)

            # Kill `runner` and all of its children.
            try:
                os.killpg(os.getpgid(p.pid), signal.SIGTERM)
            except ProcessLookupError:
                pass

            # Rejoin to prevent spawning zombie processes.
            p.join()

    def __repr__(self):
        """Return a string representing this symbolic executor.

        Returns:
            (str): A string representing this symbolic executor.
        """
        return f'SymbolicExecutor(path={self.path!r}, pc={self.solver!r})'


class RandomWalkSymbolicExecutor(SymbolicExecutor):
    """A symbolic executor that explores both branches of each conditional in random order."""
    def __init__(self, rng, exhaustive_steps = 0):
        """Create a new random-walks symbolic executor."""
        super().__init__()
        self.rng = rng
        self.random_seeds = [0, 0]
        self.exhaustive_steps = exhaustive_steps

    @override(SymbolicExecutor)
    def on_branch(self, condition, bool_ref):
        """Return a tuple describing the branches of the current condition to explore.

        Return randomly either (True, False) or (False, True).

        Returns:
            (:tuple:Bool): Either (True, False) or (False, True).
        """
        reachable_branches = super().on_branch(condition, bool_ref)
        if len(reachable_branches) == 2:
            if self.exhaustive_steps > 0:
                self.exhaustive_steps -= 1
            else:
                choice = self.rng.choice((True, False))
                del reachable_branches[not choice]

        return reachable_branches

    @override(SymbolicExecutor)
    def run(self, f, *args, **kwargs):
        # To guarantee that the choices made by spawn subprocesses are random, before branching generate two random
        # seeds from the given random number generator, and use them to reseed the subprocesses after branching.

        def prepare_random_seeds(se):
            se.random_seeds[0] = self.rng.getrandbits(32)
            se.random_seeds[1] = self.rng.getrandbits(32)

        def reseed(se, branch):
            se.rng.seed(self.random_seeds[branch])

        # Bypass `add_handler` to make sure that these handlers are called first.
        self.handlers[Event.ON_BEFORE_FORK].insert(0, prepare_random_seeds)
        self.handlers[Event.ON_AFTER_FORK].insert(0,  reseed)

        saved_timeout = self.timeout
        self.timeout = None
        super().run(f, *args, **kwargs)
        self.timeout = saved_timeout

        self.remove_handler(Event.ON_BEFORE_FORK, prepare_random_seeds)
        self.remove_handler(Event.ON_AFTER_FORK, reseed)

class PolicyGuidedSymbolicExecutor(SymbolicExecutor):
    """A symbolic executor guided by a policy."""
    def __init__(self, policy, history_size):
        super().__init__()
        self.policy = policy
        self.history_size = history_size

    @override(SymbolicExecutor)
    def handle_path(self, condition, branch):
        self.path.append(Decision(condition, branch))

    @override(SymbolicExecutor)
    def on_branch(self, condition, bool_ref):
        reachable_branches = super().on_branch(condition, bool_ref)
        if len(reachable_branches) < 2:
            return reachable_branches

        history = tuple(self.path.decisions[-(self.history_size + 1):-1])
        options = self.policy[condition, history]
        if len(options) == 1:
            option, = options
            logger.debug('policy forces deterministic choice: %r', option)
            del reachable_branches[not option]

        return reachable_branches
