import inspect
import os
import sys
import time

from functools import wraps


class StopTracing(Exception):
    """Raised by `Tracer.trace` to signal that the execution of the traced function has ended."""
    pass


class Tracer(object):
    """Class to trace the number of lines executed by a callable."""

    def __init__(self):
        """Create a new tracer.

        The new tracer has not counted any instruction.
        """

        self.nr_instr = 0

    def skip(self, frame, event, arg):
        """Decide whether to count the current line.

        This method is called before executing each line of the traced function.
        If it returns `True` the current line is not counted.

        Args:
            frame (frame): The `frame` parameter of the tracing function.
            event (str): The `event` parameter of the tracing function.
            arg (object): The `arg` parameter of the tracing function.

        Returns:
            (bool): Always `False`.
        """
        return False

    def stop(self, frame, event, arg):
        """Decide whether to stop the tracing.

        This method is called after counting a line of the traced function.
        If it returns `True` the tracing stops.

        Args:
            frame (frame): The `frame` parameter of the tracing function.
            event (str): The `event` parameter of the tracing function.
            arg (object): The `arg` parameter of the tracing function.

        Returns:
            (bool): Always `False`.
        """
        return False

    def trace(self, f, *args, **kwargs):
        """Run and trace the given function on the given parameters.

        Notes:
            The number of lines executed is accumulated in `self.nr_instr`.

        Args:
            f (function): The function to trace.
            *args (:list:object): The positional parameters passed to `f`.
            **kwargs (:dict:object): The keyword parameters passed to `f`.
        """
        def tracer(frame, event, arg):
            if self.skip(frame, event, arg):
                return tracer

            if event == 'line':
                self.nr_instr += 1

            if self.stop(frame, event, arg):
                raise StopTracing()

            return tracer

        # Reset the number of instructions traced so far.
        self.nr_instr = 0

        # Run and trace the execution of `f` with the custom tracer.
        # The number of lines executed by `f` is accumulated in `self.nr_instr`.
        sys.settrace(tracer)
        try:
            return f(*args, **kwargs)
        finally:
            sys.settrace(None)


class TimeoutTracer(Tracer):
    """A tracer that traces a function only for a limited number of seconds."""
    def __init__(self, timeout):
        """Create a timeout tracer.

        Args:
            timeout (float): The timeout, in seconds.
        """
        super().__init__()
        self.timeout = timeout
        self.start = None

    def stop(self, frame, event, arg):
        """Return `True` after `self.timeout` seconds have elapsed.

        Args:
            frame (frame): The `frame` parameter of the tracing function.
            event (str): The `event` parameter of the tracing function.
            arg (object): The `arg` parameter of the tracing function.

        Returns:
            (bool): `True` after `self.timeout` seconds have elapsed.
        """
        return (time.time() - self.start) >= self.timeout

    def trace(self, f, *args, **kwargs):
        self.start = time.time()
        super().trace(f, *args, **kwargs)


class LimitedTracer(Tracer):
    """A tracer that traces a function for a limited number of instructions."""
    def __init__(self, max_instr):
        """Create a limited tracer.

        Args:
            max_instr (int): The maximum number of instructions to trace.
        """
        super().__init__()
        self.max_instr = max_instr

    def stop(self, frame, event, arg):
        """Return `True` after `self.max_instr` instructions have been traced.

        Args:
            frame (frame): The `frame` parameter of the tracing function.
            event (str): The `event` parameter of the tracing function.
            arg (object): The `arg` parameter of the tracing function.

        Returns:
            (bool): `True` after `self.max_instr` instructions have been traced.
        """
        return self.nr_instr >= self.max_instr


class FunScopeTracer(Tracer):
    """A tracer that traces a function only within its package."""

    def __init__(self):
        """Create a fun-scope tracer."""
        super().__init__()
        self.package_path = None

    def skip(self, frame, event, arg):
        """Return `True` if outside the original traced package.

        Args:
            frame (frame): The `frame` parameter of the tracing function.
            event (str): The `event` parameter of the tracing function.
            arg (object): The `arg` parameter of the tracing function.

        Returns:
            (bool): `True` if outside the original traced package.
        """
        return os.path.dirname(inspect.getfile(frame)) != self.package_path

    def trace(self, f, *args, **kwargs):
        self.package_path = os.path.dirname(inspect.getfile(inspect.getmodule(f)))
        return super().trace(f, *args, **kwargs)


def traced(tracer):
    """"Decorate a function for tracing with the given tracer.

    When ran on an input, the decorated function returns a pair (original-output, number-of-lines).

    Args:
        tracer (Tracer): The tracer to trace the decorated function.
    """
    def wrap(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            output = tracer.trace(f, *args, **kwargs)
            return output, tracer.nr_instr
        return wrapper
    return wrap


def untraceable(f):
    """Decorate a function to not be traced.

    The decorated function is not traced. The tracing is restored at the exit of the decorated function.

    Args:
        f (function): The function to decorate.
    """
    @wraps(f)
    def wrapper(*args, **kwargs):
        tracer = sys.gettrace()
        sys.settrace(None)
        try:
            return f(*args, **kwargs)
        finally:
            sys.settrace(tracer)
    return wrapper
