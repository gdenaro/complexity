import json
import os
import sys
import subprocess
import functools

import logbook
import click
import yaml


EXPERIMENTS_OUTPUTS_CSV_FILE_NAME = 'experiments_outputs.csv'
EVOLUTION_CSV_FILE_NAME = '{method_}-{algorithm_}-{size_}-{run_}.csv'


logger = logbook.Logger('experiments')
stdout_logger = logbook.Logger('stdout')
logbook.StreamHandler(sys.stderr).push_application()


def execute_command(command, logger, stdout_logger):
    logger.info('command: ' + ' '.join(command))

    process = subprocess.Popen(
        command,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
        universal_newlines=True,
    )

    for line in process.stdout:
        stdout_logger.debug(line.rstrip())

    return_code = process.wait()

    return return_code


def parse_yaml(yaml_file_path):
    with open(yaml_file_path, mode='r', encoding='utf-8') as yaml_file:
        content = yaml.load(yaml_file)
    return content


def check_status(file_path, method, algorithm, size, run):
    if not os.path.exists(file_path):
        return False

    with open(file_path, 'r') as json_file:
        status = json.load(json_file)
        experiment_id = f'{method}-{algorithm}-{size}-{run}'
        return status[experiment_id] if experiment_id in status else False


def update_status(file_path, method, algorithm, size, run):
    status = {}
    if os.path.exists(file_path):
        with open(file_path, 'r') as json_file:
            status = json.load(json_file)

    experiment_id = f'{method}-{algorithm}-{size}-{run}'
    status[experiment_id] = True

    with open(file_path, 'w') as json_file:
        json.dump(status, json_file, indent=4)


def common_parameters(func):
    @click.option('--configuration-file', '-c', 'configuration_file', type=click.Path(exists=True), required=True)
    @click.option('--output-directory', '-o', 'output_directory', type=click.Path(exists=True), required=True)
    @click.option('--status-file', '-p', 'status_file', type=click.Path(), required=True)
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        return func(*args, **kwargs)
    return wrapper


@click.group()
def cli():
    pass


@cli.command()
@common_parameters
def bruteforce(configuration_file, output_directory, status_file):
    configuration = parse_yaml(configuration_file)
    experiments_outputs_csv_file_path = os.path.join(output_directory, EXPERIMENTS_OUTPUTS_CSV_FILE_NAME)

    for size in configuration['sizes']:
        for algorithm in configuration['algorithms']:
            for run in range(configuration['runs']):
                if check_status(status_file, 'brute_force', algorithm['function'], size, run):
                    continue

                return_code = execute_command(
                    [
                        'wcet',
                        'bruteforce',
                        algorithm['module'],
                        algorithm['function'],
                        str(size),
                        '--timeout', str(configuration['timeout']),
                        '--experiments-outputs-csv', experiments_outputs_csv_file_path,
                    ],
                    logger,
                    stdout_logger,
                )

                print(return_code)

                if return_code == 0:
                    update_status(status_file, 'brute_force', algorithm['function'], size, run)


@cli.command()
@common_parameters
def wise(configuration_file, output_directory, status_file):
    configuration = parse_yaml(configuration_file)
    experiments_outputs_csv_file_path = os.path.join(output_directory, EXPERIMENTS_OUTPUTS_CSV_FILE_NAME)

    for size in configuration['sizes']:
        for algorithm in configuration['algorithms']:
            for run in range(configuration['runs']):
                if check_status(status_file, 'wise', algorithm['function'], size, run):
                    continue

                return_code = execute_command(
                    [
                        'wcet',
                        'wise',
                        algorithm['module'],
                        algorithm['function'],
                        str(size),
                        '--timeout', str(configuration['timeout']),
                        '--experiments-outputs-csv', experiments_outputs_csv_file_path,
                        '--min-training-size', str(0),
                        '--max-training-size', str(size - 1),
                    ],
                    logger,
                    stdout_logger,
                )

                print(return_code)

                if return_code == 0:
                    update_status(status_file, 'wise', algorithm['function'], size, run)


@cli.command()
@common_parameters
def corina(configuration_file, output_directory, status_file):
    configuration = parse_yaml(configuration_file)
    experiments_outputs_csv_file_path = os.path.join(output_directory, EXPERIMENTS_OUTPUTS_CSV_FILE_NAME)

    for size in configuration['sizes']:
        for algorithm in configuration['algorithms']:
            for run in range(configuration['runs']):
                if check_status(status_file, 'corina', algorithm['function'], size, run):
                    continue

                return_code = execute_command(
                    [
                        'wcet',
                        'corina',
                        algorithm['module'],
                        algorithm['function'],
                        str(size),
                        '--timeout', str(configuration['timeout']),
                        '--experiments-outputs-csv', experiments_outputs_csv_file_path,
                        '--min-training-size', str(0),
                        '--max-training-size', str(size - 1),
                    ],
                    logger,
                    stdout_logger,
                )

                print(return_code)

                if return_code == 0:
                    update_status(status_file, 'corina', algorithm['function'], size, run)


@cli.command()
@common_parameters
def genetic(configuration_file, output_directory, status_file):
    configuration = parse_yaml(configuration_file)
    experiments_outputs_csv_file_path = os.path.join(output_directory, EXPERIMENTS_OUTPUTS_CSV_FILE_NAME)

    for size in configuration['sizes']:
        for algorithm in configuration['algorithms']:
            for run in range(configuration['runs']):
                if check_status(status_file, f'genetic_algorithm_{configuration["crossover"]}', algorithm['function'], size, run):
                    continue

                evolution_csv_file_path = os.path.join(output_directory, EVOLUTION_CSV_FILE_NAME.format(
                    method_=f'genetic_algorithm_{configuration["crossover"]}',
                    algorithm_=algorithm['function'],
                    size_=size,
                    run_=run,
                ))

                return_code = execute_command(
                    [
                        'wcet',
                        'genetic',
                        algorithm['module'],
                        algorithm['function'],
                        str(size),
                        '--timeout', str(configuration['timeout']),
                        '--experiments-outputs-csv', experiments_outputs_csv_file_path,
                        '--seed', str(run),
                        '--population-size', str(configuration['population_size']),
                        '--generations', str(configuration['generations']),
                        '--crossover', str(configuration['crossover']),
                        '--mutation-probability', str(configuration['mutation_probability']),
                        '--elitism-rate', str(configuration['elitism_rate']),
                        '--processors', str(configuration['processors']),
                        '--evolution-csv', evolution_csv_file_path
                    ],
                    logger,
                    stdout_logger,
                )

                print(return_code)

                if return_code == 0:
                    update_status(status_file, f'genetic_algorithm_{configuration["crossover"]}', algorithm['function'], size, run)


@cli.command()
@common_parameters
def random(configuration_file, output_directory, status_file):
    configuration = parse_yaml(configuration_file)
    experiments_outputs_csv_file_path = os.path.join(output_directory, EXPERIMENTS_OUTPUTS_CSV_FILE_NAME)

    for size in configuration['sizes']:
        for algorithm in configuration['algorithms']:
            for run in range(configuration['runs']):
                if check_status(status_file, 'random_search', algorithm['function'], size, run):
                    continue

                evolution_csv_file_path = os.path.join(output_directory, EVOLUTION_CSV_FILE_NAME.format(
                    method_='random_search',
                    algorithm_=algorithm['function'],
                    size_=size,
                    run_=run,
                ))

                return_code = execute_command(
                    [
                        'wcet',
                        'genetic',
                        algorithm['module'],
                        algorithm['function'],
                        str(size),
                        '--timeout', str(configuration['timeout']),
                        '--experiments-outputs-csv', experiments_outputs_csv_file_path,
                        '--seed', str(run),
                        '--population-size', str(configuration['population_size']),
                        '--generations', str(configuration['generations']),
                        '--mutation-probability', str(configuration['mutation_probability']),
                        '--elitism-rate', str(configuration['elitism_rate']),
                        '--processors', str(configuration['processors']),
                        '--random-search',
                        '--evolution-csv', evolution_csv_file_path
                    ],
                    logger,
                    stdout_logger,
                )

                print(return_code)

                if return_code == 0:
                    update_status(status_file, 'random_search', algorithm['function'], size, run)


if __name__ == '__main__':
    cli()
