import unittest

from complexity.structs import slist
from complexity.executors.genetic_executor import *
from complexity.utils import pp_wcet_profiles
from complexity.case_studies.gecco.memory_fill import memory_fill


logger = logging.getLogger(__name__)
formatter = logging.Formatter(fmt='[%(asctime)s] %(message)s', datefmt='%m/%d/%Y %H:%M:%S')
handler = logging.StreamHandler()
handler.setFormatter(formatter)

logger.addHandler(handler)
logger.setLevel(logging.DEBUG)


class TestGenetic(unittest.TestCase):
    def test_run(self):
        ga_configuration = GAConfig(
            generations=10,
            population_size=10,
            crossover='singlepoint',
            mutation_prob=0.2,
            elite_ratio=0.1,
            timeout=5,
            pool_size=1,
            seed=0,
            random_search=None,
            evolution_csv=None,
        )

        profiles = ga_wcet_generator(
            memory_fill,
            args=(slist('l', 60),),
            kwargs={},
            config=ga_configuration,
        )

        pp_wcet_profiles(profiles)

        return profiles[0].model, profiles[0].cost
