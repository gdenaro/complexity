import unittest
import z3

from complexity.structs import *


class TestStructs(unittest.TestCase):
    def test_empty_slist(self):
        self.assertEqual(len(slist('l', 0)), 0)

    def test_slist_with_ten_elements(self):
        self.assertEqual(len(slist('l', 10)), 10)

    def test_slist_content(self):
        self.assertTrue(isinstance(slist('l', 10)[0], z3.ArithRef))
