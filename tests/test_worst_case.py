import unittest
import z3

from complexity.structs import slist
from complexity.utils import assert_all_different
from complexity.executors.bruteforce_executor import *


def waste_cyles(n):
    for i in range(n):
        pass


class TestWorstCase(unittest.TestCase):
    def test_path_profiles(self):
        def test(a, b):
            min_ = min(a, b)
            if min_ == 3:
                max_ = max(a, b)
                if max_ == 4:
                    x = 0
                    x += 1
                    return 1
            return 2

        se = SymbolicExecutor()
        profiles = path_profiles(se, test, (z3.Int('a'), z3.Int('b')), {})
        self.assertEqual(sum(p.complete for p in profiles), 7)

    def test_wcetpp(self):
        def test(a, b):
            if min(a, b) == 3 and max(a, b) == 4:
                x = 0
                x += 1
                return 1
            else:
                return 2

        se = SymbolicExecutor()
        profiles = wcetpp(se, test, (z3.Int('a'), z3.Int('b')), {})
        self.assertEqual(profiles[0].cost, 4)
        self.assertEqual(profiles[1].cost, 4)

        sol1 = {'a': 3, 'b': 4}
        sol2 = {'a': 4, 'b': 3}
        self.assertTrue(profiles[0].model in (sol1, sol2))
        self.assertTrue(profiles[1].model in (sol1, sol2))

    def test_wcetpp_with_precondition(self):
        def test(x):
            if x > 0:
                waste_cyles(n=100)
                return 1
            return 2

        se = SymbolicExecutor()
        se.preconditions.append(z3.Int('x') > 10)
        profiles = wcetpp(se, test, (z3.Int('x'),), {})
        self.assertEqual(len(profiles), 1)
        self.assertEqual(profiles[0].pc, '(declare-fun x () Int)(assert (> x 0))')
        self.assertTrue(profiles[0].model['x'] > 10)

    def test_inc_wcetpp(self):
        def all_positive(l):
            for e in l:
                if e <= 0:
                    return False

            waste_cyles(n=100)
            return True

        def args_gen(size):
            return slist('l', size),

        def kwargs_gen(size):
            return {}

        def preconditions_gen(se, args, kwargs):
            return [assert_all_different(args[0])]

        inc_profiles = inc_wcetpp(
            all_positive,
            args_gen,
            kwargs_gen,
            preconditions_gen,
            min_size=1,
            max_size=5,
            timeout=60
        )

        self.assertEqual(len(inc_profiles), 5)
        for size, profiles in inc_profiles:
            self.assertTrue(len(profiles) > 0)
            self.assertEqual(len(profiles[0].model), size)

            for value in profiles[0].model.values():
                self.assertTrue(value > 0)

    def test_analyze_wcet(self):
        def f(l):
            for e in l:
                if e < 100:
                    return 0

            waste_cyles(n=100)
            return 1

        def args_gen(size):
            return slist('l', size),

        def kwargs_gen(size):
            return {}

        def preconditions_gen(size, args, kwargs):
            return []

        profiles = analyze_wcet(
            f,
            args_gen,
            kwargs_gen,
            preconditions_gen,
            min_training_size=0,
            max_training_size=3,
            timeout=60,
            target_size=15,
            history_size=0,
        )

        self.assertTrue(len(profiles) > 0)
        for profile in profiles:
            self.assertEqual(len(profile.model), 15)
            self.assertTrue(all(value >= 100 for key, value in profile.model.items()))
